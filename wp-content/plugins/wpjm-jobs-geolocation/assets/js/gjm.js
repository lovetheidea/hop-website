jQuery(document).ready(function($) {
    
    //default values
    var thisPrefix       = 'gjm';
    var thisForm         = false;
    var thisTarget       = false;
    var addressField     = false;
    var prevAddressField = false;
    var latField         = false;
    var lngField         = false;
    var autoLocated      = false;
    var enterClicked     = false;


    //move the locator button to within the address field
    $( '.gjm-locator-btn-holder, .grm-locator-btn-holder' ).each(function() {
        $(this).children().detach().appendTo( $(this).closest('form').find('.search_location') );
    })

    //if auto locator set for page load we need to cancel the initial
    //Jobs form on page load so we can do out search based on the current position
    $('.gjm-autolocator-trigger, .grm-autolocator-trigger').each( function() {
        var thisElement = $(this);
        $( window ).on('load').on( 'load', ( function () {
            autoLocated = true;
            locatorTrigger( thisElement );
        }));     
    });

    //if using results map shortcode elsewhere on the page remove the map from its original place
    //which is above the list of results into the new map holder.
    $('.gjm-map-wrapper, .grm-map-wrapper').each(function() {

        thisForm   = $(this).closest( 'form' );
        thisPrefix = thisForm.find('#gjm_prefix').val();
        mapId      = $(this).attr('data-map-id');
        mapHolder  = $('#'+thisPrefix+'-results-map-holder-'+mapId);

        if ( $('#'+thisPrefix+'-results-map-holder-'+mapId).length ) {

            $(this).detach().appendTo('#'+thisPrefix+'-results-map-holder-'+mapId);
        } else if ( $('.results-map-holder' ).length ) {
            $(this).detach().appendTo('.results-map-holder');
        }
    });

    //reset form fields
    $( '.job_filters, .resume_filters' ).on( 'click', '.reset', function (e) {
        thisForm   = $( this ).closest( 'form' );   
        thisForm.find('.gjm-filter option:first-child, .grm-filter option:first-child').attr("selected", "selected");
        thisForm.find('#gjm_lat, #gjm_lng, #prev_address').val('')
    } );
      
    //hide map if no results found  
    $( '.job_listings' ).on( 'updated_results', function( results ) {
         
        //temporary solution to check if "No results" after search submitted.
        //Until we find a "cleaner" way.
        if ( results.target.lastChild.innerHTML.indexOf('no_job_listings_found') > -1 )  {

            thisPrefix = $(this).find('#gjm_prefix').val();
            mapId      = $(this).find('#gjm_element_id').val();

            $('#'+thisPrefix+'-map-wrapper-'+mapId+':visible').slideToggle();
        }
    });

    // prevent filter from executing twice when enter key is pressed
    $( '.gjm-enabled' ).closest( 'form' ).find( ' #search_location' ).keydown( function( event ){

        if ( event.keyCode == 13 ){

            event.preventDefault();
            return false;
        }
    });

    $( '.gjm-enabled' ).closest( 'form' ).find( '#search_keywords, #search_location, .job_types :input, #search_categories, .job-manager-filter' ).off('change').on('change', ( function ( event ) {
        
        //if ( enterClicked == true && event.currentTarget.id  == 'asd') {
          //  enterClicked = false;
            //return false;
        //}

        console.log( '1' )
        //default values
        thisForm         = $(this).closest( 'form' );
        thisTarget       = $(this).closest( 'div.job_listings, div.resumes' );
        addressField     = thisForm.find('#search_location');
        prevAddressField = thisForm.find('#gjm_prev_address');
        latField         = thisForm.find('#gjm_lat');
        lngField         = thisForm.find('#gjm_lng');

        //set timeout to let enough time for the address autocomplete value to set in the location field.
        //This is kind of a "hack" since the change event happesn faster then the address value can be set for the field
        //which results in a "currapted" address value.
        setTimeout( function() {
            
            newAddress  = addressField.val();
            prevAddress = thisForm.find('#gjm_prev_address').val();
            addressYes  = ( $.trim( newAddress ).length ) ? true : false;

            // if no address entered
            if ( ! addressYes ) {
                
                //alert('no address');

                latField.val('');
                lngField.val('');
                prevAddressField.val('');
                
                thisTarget.triggerHandler( 'update_results', [ 1, false ] );

            // if address entered
            } else if ( addressYes ) {
                
                //dynamically set the radius dropdown to the last option
                //if address entered and the radius dropdown value selected is default
                if ( $(".gjm-radius option:first-child").is(":selected") ) {
                    $(".gjm-radius option:last-child").attr('selected', 'selected');
                }
                
                //if address changed or no coordinates exist we will geocode the address
                //we check if the address changed using the prev_address hidden field
                if ( ( newAddress != prevAddress ) || ( latField == '' && lngField == '' ) ) {

                    //alert('new address '+newAddress);
                     
                    thisForm.find( '.gjm-country-code, .gjm-country-name, .gjm-state-name, .gjm-state-code' ).val( '' );
               
                    //set the new address in the prev_address field
                    prevAddressField.val( newAddress );

                    //geocode the address
                    var geocoder = new google.maps.Geocoder();

                    geocoder.geocode( {'address': newAddress }, function(results, status) {
                        
                        //if geocoding successful
                        if ( status == google.maps.GeocoderStatus.OK ) {

                            // if only country entered set its value in hidden fields
                            if ( results[0].address_components.length == 1 && results[0].address_components[0].types[0] == 'country' ) {
                                
                                thisForm.find( '.gjm-country-name' ).val( results[0].address_components[0].long_name );
                                thisForm.find( '.gjm-country-code' ).val( results[0].address_components[0].short_name );

                            // otherwise, if only state entered.
                            } else if ( results[0].address_components.length == 2 && results[0].address_components[0].types[0] == 'administrative_area_level_1' ) {
                                
                                thisForm.find( '.gjm-country-name' ).val( results[0].address_components[1].long_name );
                                thisForm.find( '.gjm-country-code' ).val( results[0].address_components[1].short_name );

                                thisForm.find( '.gjm-state-name' ).val( results[0].address_components[0].long_name );
                                thisForm.find( '.gjm-state-code' ).val( results[0].address_components[0].short_name );
                            }

                            //set the coordinates
                            latField.val( results[0].geometry.location.lat() );
                            lngField.val( results[0].geometry.location.lng() );

                            //run the ajax search
                            //give it a bit of extra time to set the coordinates in the hidden fields
                            setTimeout( function() {
                                thisTarget.triggerHandler( 'update_results', [ 1, false ] );
                            }, 500 );
                        
                        //if geocoding failed
                        } else {

                            latField.val('');
                            lngField.val('');
                            prevAddressField.val('');

                            //if address could not geocoded abort the function and display an error message
                            alert( 'We could not find the address you entered. Please try a different address. Error message: '+status);     
                        }
                    });

                //if the same address entered we should be able to use the previous coords again.
                } else {
                    //alert('same address');
                    thisTarget.triggerHandler( 'update_results', [ 1, false ] );
                }
            } 
         }, 500 );

        return false;    
    }));

    //refresh form when changing gjm, grm values
    $('.gjm-filter, .grm-filter').change(function() {

        thisForm      = $(this).closest('form');
        locationField = thisForm.find( '#search_location' );

        if ( ( $(this).hasClass('radius-filter') || $(this).hasClass('units-filter') ) && locationField.val() == '' )
            return;

        thisTarget = $(this).closest('div.job_listings, div.resumes');

        thisTarget.trigger('update_results', [1, false]);
    });

    /**
     * Google Address Autocomplete
     * @param  {[type]}           
     * @return void
     */
    $('.gjm-autocomplete-trigger, .grm-autocomplete-trigger').each(function() {
       
        // get the form element
        thisForm = $(this).closest('form');
        // get autocomplete field
        acField = $(this).closest('form').find('#search_location')[0];
        // get restricted country 
        acCountry = $(this).attr('data-country');
        // results type
        resultsType = $(this).attr('data-results-type');
       
        var acOptions = {
            types : [resultsType]
        };

        if ( acCountry != '' ) {
            acOptions.componentRestrictions = { country : acCountry }
        } 

        var acObject = new google.maps.places.Autocomplete( acField , acOptions );
        
        google.maps.event.addListener( acObject, 'place_changed', function(e) {

            thisForm.find( '.gjm-country-code, .gjm-country-name, .gjm-state-name, .gjm-state-code' ).val( '' );
               
            var place = acObject.getPlace();

            if ( ! place.geometry )
                return;

            // if only country entered set its value in hidden fields
            if ( place.address_components.length == 1 && place.address_components[0].types[0] == 'country' ) {
                                
                thisForm.find( '.gjm-country-name' ).val( place.address_components[0].long_name );
                thisForm.find( '.gjm-country-code' ).val( place.address_components[0].short_name );

            // otherwise if only state entered.
            } else if ( place.address_components.length == 2 && place.address_components[0].types[0] == 'administrative_area_level_1' ) {
                
                thisForm.find( '.gjm-country-name' ).val( place.address_components[1].long_name );
                thisForm.find( '.gjm-country-code' ).val( place.address_components[1].short_name );

                thisForm.find( '.gjm-state-name' ).val( place.address_components[0].long_name );
                thisForm.find( '.gjm-state-code' ).val( place.address_components[0].short_name );
            }

            //update coords fields
            thisForm.find( '#gjm_lat' ).val( place.geometry.location.lat() );
            thisForm.find( '#gjm_lng' ).val( place.geometry.location.lng() );

            //update prev_field to be the same as address field. This way
            //we don't need to geocode the address again since we have coordinates already
            thisForm.find( '#gjm_prev_address' ).val( thisForm.find( '#search_location' ).val() )       
        });
    });       

    /**
     * locator button
     */
    $('.gjm-locator-btn, .grm-locator-btn').click(function() {
        autoLocated = false;
        locatorTrigger( $(this) );
    });

    function locatorTrigger( thisElement ) {

        var locatorBtn    = thisElement;
        var locatorOk     = false;
        thisForm          = thisElement.closest('form');
        thisPrefix        = thisForm.find('#gjm_prefix').val();
        thisTarget        = thisElement.closest( 'div.job_listings, div.resumes' );
        addressField      = thisForm.find('#search_location');
        prevAddressField  = thisForm.find('#gjm_prev_address');
        latField          = thisForm.find('#gjm_lat');
        lngField          = thisForm.find('#gjm_lng');
        var locatorLoader = thisForm.find('.'+thisPrefix+'-locator-loader');

        //hide locator button and show loader 
        thisForm.find('.'+thisPrefix+'-locator-btn').fadeToggle('fast', function() {
            locatorLoader.fadeToggle('fast');
        });

        // if GPS exists locate the user //
        if ( navigator.geolocation ) {
            navigator.geolocation.getCurrentPosition(showPosition, showError, { timeout: 10000 } );
        } else {
            // if nothing found we cant do much. we cant locate the user :( //
            alert('Sorry! Geolocation is not supported by this browser and we cannot locate you.');
        }

        // GPS locator function //
        function showPosition(position) {

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({'latLng': new google.maps.LatLng(position.coords.latitude, position.coords.longitude)}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    prevAddressField.val(results[0].formatted_address);
                    addressField.val(results[0].formatted_address);

                    latField.val(results[0].geometry.location.lat());
                    lngField.val(results[0].geometry.location.lng());
                    
                    //run the ajax search
                    //give it a bit of extra time to set the coordinates in the hidden fields
                    setTimeout(function() {

                        //thisTarget.triggerHandler( 'update_results', [ 1, false ] );
                        addressField.change();
                        
                        //hide locator button and show loader 
                        locatorLoader.fadeToggle('fast', function() {
                            thisForm.find('.'+thisPrefix+'-locator-btn').fadeToggle('fast');
                        });
                    }, 500 );

                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        }

        function showError(error) {

            if ( ! autoLocated ) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        alert('User denied the request for Geolocation.');
                        break;
                    case error.POSITION_UNAVAILABLE:
                        alert('Location information is unavailable.');
                        break;
                    case 3:
                        alert('The request to get user location timed out.');
                        break;
                    case error.UNKNOWN_ERROR:
                        alert('An unknown error occurred');
                        break;
                }
            }
            //hide locator button and show loader 
            locatorLoader.fadeToggle('fast', function() {
                thisForm.find('.'+thisPrefix+'-locator-btn').fadeToggle('fast');
            });
        }
    };
});