<?php

class PeepSoConfigSectionAdvanced extends PeepSoConfigSectionAbstract
{
	public static $css_overrides = array(
		'appearance-avatars-circle',
	);

	// Builds the groups array
	public function register_config_groups()
	{
		$this->context='full';
		$this->_group_filesystem();

		if( !isset($_GET['filesystem']) ) {
            $this->_group_uninstall();
            $this->_group_profiles();

			$this->context = 'left';
            $this->_group_opengraph();
            $this->_group_debug();

			$this->context = 'right';
			$this->_group_listings();
            $this->_group_ajax();
            $this->_group_storage();
            $this->_group_security();
			$this->_group_emails();
		}

		# @todo #257 $this->config_groups[] = $this->_group_opengraph();
	}

	private function _group_profiles() {

	    $tabs = apply_filters('peepso_navigation_profile', array());
	    $tablist='';
	    foreach($tabs as $id=>$tab) {
	        if(in_array($tab,array('stream','about'))) {
                $tablist.="<br>$id";
	            continue;
            }

            $tablist.="<br><strong>$id</strong>";
        }

        $this->args('raw', TRUE);
        $this->args('descript', sprintf(__('One tab name per line. "Stream" and "About" will always be first. Current order: %s', 'peepso-core'), $tablist));

        $this->set_field(
            'profile_tabs_order',
            __('Profile tabs order (beta)', 'peepso-core'),
            'textarea'
        );


        $this->set_group(
            'profiles',
            __('Profiles', 'peepso-core')
        );
    }

	private function _group_filesystem()
	{

		// Message Filesystem
		$this->set_field(
			'system_filesystem_warning',
			__('This setting is to be changed upon very first PeepSo activation or in case of site migration. If changed in any other case it will result in missing content including user avatars, covers, photos etc. (error 404).', 'peepso-core'),
			'warning'
		);

		// Message Filesystem
		$this->set_field(
			'system_filesystem_description',
			__('PeepSo allows users to upload images that are stored on your server. Enter a location where these files are to be stored.<br/>This must be a directory that is writable by your web server and and is accessible via the web. If the directory specified does not exist, it will be created.', 'peepso-core'),
			'message'
		);

		$this->args('class','col-xs-12');
		$this->args('field_wrapper_class','controls col-sm-10');
		$this->args('field_label_class', 'control-label col-sm-2');
		$this->args('default', WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'peepso');

		$this->args('validation', array('required', 'custom'));
		$this->args('validation_options',
			array(
			'error_message' => __('Can not write to directory', 'peepso-core'),
			'function' => array($this, 'check_wp_filesystem')
			)
		);
		// Uploads
		$this->set_field(
			'site_peepso_dir',
			__('Uploads Directory', 'peepso-core'),
			'text'
		);

		$this->set_group(
			'filesystem',
			__('File System', 'peepso-core')
		);
	}

	private function _group_debug()
	{
		// Logging
        $this->args('descript', __('Enabled: various debug information is written to a log file.','peepso-core').'<br/>'.__('This can impact website speed and should ONLY be enabled when someone is debugging PeepSo.', 'peepso-core'));
		$this->set_field(
			'system_enable_logging',
			__('Enable Logging', 'peepso-core'),
			'yesno_switch'
		);

        // FSTVL
        $this->args('descript', __('Strict Version Lock makes sure that it\'s impossible to upgrade PeepSo Core before all of the child plugins have been updated.','peepso-core').'<br/>'.__('Please DO NOT enable this unless you are having issues with updating PeepSo Core.', 'peepso-core'));
        $this->set_field(
            'override_fstvl',
            __('Override Strict Version Lock', 'peepso-core'),
            'yesno_switch'
        );

		$this->set_group(
			'advanced_debug',
			__('Maintenance & debugging', 'peepso-core')
		);
	}

	/**
	 * #1922 add config option for listings
	 */
	private function _group_listings()
	{
	    // Infinite load
		$this->args('descript', __('Disables infinite loading of activities, members lists etc until the users clicks the "load more" button.', 'peepso-core'));
		$this->set_field(
			'loadmore_enable',
			__('Enable "load more:" button', 'peepso-core'),
			'yesno_switch'
		);

		$this->set_group(
			'listings',
			__('Listings', 'peepso-core')
		);
	}

    private function _group_ajax()
    {
        // Notification AJAX delay
        $this->args('default', 30000);

        for($i = 5000; $i<=240000; $i+=5000){
            $options[$i]= sprintf(_n('%d second','%d seconds', ($i/1000), 'peepso-core'), ($i/1000));
            if($i == 30000) {
                $options[$i] .= ' ('.__('default', 'peepso-core').')';
            }
        }

        $this->args('options', $options);

        $this->args('descript', __('How often to run  new notification check for each user that is logged in','peepso_core') .'<br>'.__('Lower number means more robust notifications, but also higher server load','peepso-core'));
        $this->set_field(
            'notification_ajax_delay',
            __('AJAX call intensity', 'peepso-core'),
            'select'
        );

        // Build Group
        $this->set_group(
            'ajax',
            __('Performance', 'peepso-core')
        );
    }

    private function _group_storage()
    {
        // Avatar size
        $this->args('default', 100);

        $options=array();
        for($i = 100; $i<=500; $i+=50){
            $options[$i]= sprintf(__('%d pixels', 'peepso-core'), $i);
            if($i == 100) {
                $options[$i] .= ' ('.__('default', 'peepso-core').')';
            }
        }

        $this->args('options', $options);

        $this->args('descript', __('Bigger images use more storage, but will look better - especially on high resolution screens.','peepso_core'));
        $this->set_field(
            'avatar_size',
            __('Avatar size', 'peepso-core'),
            'select'
        );

        // Avatar quality
        $this->args('default', 75);

        $options=array();
        for($i = 50; $i<=100; $i+=5){
            $options[$i]= sprintf(__('%d%%', 'peepso-core'), $i);
            if($i == 75) {
                $options[$i] .= ' ('.__('default', 'peepso-core').')';
            }
        }

        $this->args('options', $options);

        $this->args('descript', __('Higher quality will use more storage, but the images will look better','peepso_core'));
        $this->set_field(
            'avatar_quality',
            __('Avatar quality', 'peepso-core'),
            'select'
        );

        // Build Group
        $this->set_group(
            'storage',
            __('Storage', 'peepso-core'),
            __('These settings control the dimensions and compression levels, and will only be applied to newly uploaded images.', 'peepso-core')
        );
    }

    private function _group_security()
    {
        // non-SSL embeds
        $this->args('descript', __('Enables non-SSL (http://) link fetching. This can lead to "insecure content" warnings if your site is using SSL','peepso-core'));
        $this->set_field(
            'allow_non_ssl_embed',
            __('Allow non-SSL embeds', 'peepso-core'),
            'yesno_switch'
        );

        // Build Group
        $this->set_group(
            'ajax',
            __('Security', 'peepso-core')
        );
    }

	private function _group_emails()
	{
		// # Email Sender
		$this->args('validation', array('required','validate'));
		$this->args('data', array(
			'rule-min-length' => 1,
			'rule-max-length' => 64,
			'rule-message'    => __('Should be between 1 and 64 characters long.', 'peepso-core')
		));


		$this->set_field(
			'site_emails_sender',
			__('Email sender', 'peepso-core'),
			'text'
		);

		// # Admin Email
		$this->args('validation', array('required','validate'));
		$this->args('data', array(
			'rule-type'    => 'email',
			'rule-message' => __('Email format is invalid.', 'peepso-core')
		));
		$this->set_field(
			'site_emails_admin_email',
			__('Admin Email', 'peepso-core'),
			'text'
		);

		// # Copyright Text
		$this->args('raw', TRUE);

		$this->set_field(
			'site_emails_copyright',
			__('Copyright Text', 'peepso-core'),
			'textarea'
		);

		// # Number of mails to process per run
		$this->args('validation', array('required','validate'));

		// new javascript validation
		$this->args('data', array(
			'rule-type'    => 'int',
			'rule-min'     => 1,
			'rule-max'     => 1000,
			'rule-message' => __('Insert number between 1 and 1000.', 'peepso-core')
		));

		$this->args('int', TRUE);

		$this->set_field(
			'site_emails_process_count',
			__('Number of mails to process per run', 'peepso-core'),
			'text'
		);

		// # Disable MailQueue
        $this->args('descript', __('Only set this to "yes" if you are experiencing issues with the default PeepSo mailqueue (some PeepSo emails not sent). ', 'peepso-core') .'<br/>'.__('You will have to set up a replacement cronjob, please refer to the documentation under the keyword "mailqueue"', 'peepso-core'));
		$this->set_field(
			'disable_mailqueue',
			__('Disable PeepSo Default Mailqueue', 'peepso-core'),
			'yesno_switch'
		);

		// Build Group
		$this->set_group(
			'emails',
			__('Emails', 'peepso-core')
		);
	}

	private function _group_uninstall()
	{
		// # Delete Posts and Comments
		$this->args('field_wrapper_class', 'controls col-sm-8 danger');

		$this->set_field(
			'delete_post_data',
			__('Delete Post and Comment data', 'peepso-core'),
			'yesno_switch'
		);

		// # Delete All Data And Settings
		$this->args('field_wrapper_class', 'controls col-sm-8 danger');

		$this->set_field(
			'delete_on_deactivate',
			__('Delete all data and settings', 'peepso-core'),
			'yesno_switch'
		);

		// Build Group
		$summary= __('When set to "YES", all <em>PeepSo</em> data will be deleted upon plugin Uninstall (but not Deactivation).<br/>Once deleted, <u>all data is lost</u> and cannot be recovered.', 'peepso-core');
		$this->args('summary', $summary);

		$this->set_group(
			'peepso_uninstall',
			__('PeepSo Uninstall', 'peepso-core'),
			__('Control behavior of PeepSo when uninstalling / deactivating', 'peepso-core')
		);
	}

	private function _group_opengraph()
	{
		$this->set_field(
			'opengraph_enable',
			__('Enable Open Graph', 'peepso-core'),
			'yesno_switch'
		);

		// Open Graph Title
		$this->set_field(
			'opengraph_title',
			__('Title (og:title)', 'peepso-core'),
			'text'
		);

		// Open Graph Title
		$this->set_field(
			'opengraph_description',
			__('Description (og:description)', 'peepso-core'),
			'textarea'
		);

		// Open Graph Image
		$this->set_field(
			'opengraph_image',
			__('Image (og:image)', 'peepso-core'),
			'text'
		);

        // Disable "?" in Profile / Group / Activity URLs
        $this->args('descript', __('This feature is currently in BETA and should be considered experimental. It will remove "?" from certain PeepSo URLs, such as "profile/?username/about".', 'peepso-core'));
        $this->set_field(
            'disable_questionmark_urls',
            __('Enable SEO Friendly links', 'peepso-core'),
            'yesno_switch'
        );

        $frontpage = get_post(get_option('page_on_front'));

        if (1 == PeepSo::get_option('disable_questionmark_urls', 0) && 'page' == get_option( 'show_on_front' ) && has_shortcode($frontpage->post_content, 'peepso_activity')) {
            $this->set_field(
                'activity_homepage_warning',
                __('You are currently using [peepso_activity] as your home page. Because of that, single activity URLs will have to contain "?" no matter what the above setting is.', 'peepso-core'),
                'message'
            );
        }


        // PeepSo::reset_query()
        $this->args('descript', __('This advanced feature causes PeepSo pages to override the global WP_Query for better SEO.','peepso-core').'<br>'.__('This can interfere with SEO plugins, so use with caution.', 'peepso-core'));
        $this->set_field(
            'force_reset_query',
            __('PeepSo can reset WP_Query', 'peepso-core'),
            'yesno_switch'
        );


		$this->set_group(
			'opengraph',
			__('SEO & Open Graph', 'peepso-core'),
			__("The Open Graph protocol enables sites shared for example to Facebook carry information that render shared URLs in a great way. Having a photo, title and description. You can learn more about it in our documentation. Just search for 'Open Graph'.", 'peepso-core')
		);
	}


	/**
	 * Checks if the directory has been created, if not use WP_Filesystem to create the directories.
	 * @param  string $value The peepso upload directory
	 * @return boolean
	 */
	public function check_wp_filesystem($value)
	{
		$form_fields = array('site_peepso_dir');
		$url = wp_nonce_url('admin.php?page=peepso_config&tab=advanced', 'peepso-config-nonce', 'peepso-config-nonce');

		if (FALSE === ($creds = request_filesystem_credentials($url, '', false, false, $form_fields))) {
			return FALSE;
		}

		// now we have some credentials, try to get the wp_filesystem running
		if (!WP_Filesystem($creds)) {
			// our credentials were no good, ask the user for them again
			request_filesystem_credentials($url, '', true, false, $form_fields);
			return FALSE;
		}

		global $wp_filesystem;

		if (!$wp_filesystem->is_dir($value) || !$wp_filesystem->is_dir($value . DIRECTORY_SEPARATOR . 'users')) {
			$wp_filesystem->mkdir($value);
			$wp_filesystem->mkdir($value . DIRECTORY_SEPARATOR . 'users');
			return TRUE;
		}

		return $wp_filesystem->is_writable($value);
	}

}