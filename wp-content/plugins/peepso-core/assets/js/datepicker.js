(function( $ ) {
	var datepickerConfig = {},
		defaultOptions = {},
		dayNamesMin;

	// Read datepicker configuration.
	if ( window.peepsodatepickerdata && peepsodatepickerdata.config ) {
		datepickerConfig = peepsodatepickerdata.config;
		dayNamesMin = _.map( datepickerConfig.daysShort, function( str ) {
			return str.replace( /^([a-z]{2}).+$/i, '$1' );
		});

		_.extend( defaultOptions, {
			monthNames: datepickerConfig.months,
			monthNamesShort: datepickerConfig.monthsShort,
			dayNames: datepickerConfig.days,
			dayNamesShort: datepickerConfig.daysShort,
			dayNamesMin: dayNamesMin,
			currentText: datepickerConfig.today,
			// clear: '',
			dateFormat: datepickerConfig.format,
			// weekStart: '',
			isRTL: datepickerConfig.rtl,
			changeMonth: true,
			changeYear: true,
			yearRange: '-100:+0',
			firstDay: 0
		});
	}

	// Attach accessibility improvement on show datepicker.
	_.extend( defaultOptions, {
		beforeShow: function( input, inst ) {
			inst.dpDiv.wrap( '<div class="peepso-datepicker" />' );
			attachAccessibilityImprovement( inst );
		},
		onClose: function( dateText, inst ) {
			removeAccessibilityImprovement( inst );
			_.delay(function() {
				inst.dpDiv.unwrap( '<div class="peepso-datepicker" />' );
			}, 500 );
		}
	});

	// Cache jQuery datepicker in case of override.
	$.jQueryDatepicker = $.datepicker;
	$.fn.jQueryDatepicker = $.fn.datepicker;

	$.fn.psDatepicker = function( options ) {
		return this.each(function() {
			var $input = $( this ),
				$btn = $input.next( 'button' ),
				evtName = 'click.psDatepicker';

			$btn.off( evtName ).on( evtName, function( e ) {
				var inst = $input.data( 'datepicker' ),
					$div = inst.dpDiv;

				e.stopPropagation();

				if ( $div.is( ':visible' ) ) {
					$input.jQueryDatepicker( 'hide' );
				} else {
					$input.jQueryDatepicker( 'show' );
				}
			});

			options = _.extend({}, defaultOptions, options || {});
			return $input.jQueryDatepicker( options );
		});
	};

	function attachAccessibilityImprovement( inst ) {
		setTimeout(function() {
			var $div = inst.dpDiv,
				today;

			$div.on( 'keydown.ps-datepicker', { inst: inst }, keyboardHandler );
			renderLabel( inst );

			today = $div.find( '.ui-datepicker-today a' )[0];
			if ( ! today ) {
				today = $div.find( '.ui-state-active' )[0] || $div.find( '.ui-state-default' )[0];
			}

			today.focus();
		}, 0 );
	}

	function removeAccessibilityImprovement( inst ) {
		inst.dpDiv.off( 'keydown.ps-datepicker' );
	}

	function keyboardHandler( e ) {
		var inst = e.data.inst,
			target = e.target,
			which = e.which,
			altKey = e.altKey;

		if ( which === 37 ) { // LEFT
			e.preventDefault();
			e.stopPropagation();
			goPreviousDay( inst, target );
		} else if ( which === 36 ) { // HOME
			e.preventDefault();
			e.stopPropagation();
			goFirstWeekDay( inst, target );
		} else if ( which === 39 ) { // RIGHT
			e.preventDefault();
			e.stopPropagation();
			goNextDay( inst, target );
		} else if ( which === 35 ) { // END
			e.preventDefault();
			e.stopPropagation();
			goLastWeekDay( inst, target );
		} else if ( which === 38 ) { // UP
			e.preventDefault();
			e.stopPropagation();
			goPreviousWeek( inst, target );
		} else if ( which === 40 ) { // DOWN
			e.preventDefault();
			e.stopPropagation();
			goNextWeek( inst, target );
		} else if ( which === 33 ) { // PAGEUP
			e.preventDefault();
			e.stopPropagation();
			if ( altKey ) {
				goNextYear( inst, target );
			} else {
				goNextMonth( inst, target );
			}
		} else if ( which === 34 ) { // PAGEDOWN
			e.preventDefault();
			e.stopPropagation();
			if ( altKey ) {
				goPreviousYear( inst, target );
			} else {
				goPreviousMonth( inst, target );
			}
		} else if ( which === 13 ) { // ENTER
			selectDate( inst, target );
		} else if ( which === 27 ) { // ESCAPE
			closeDatepicker( inst );
		}
	}

	function goPreviousDay( inst, target ) {
		var $currCell = $( target ).closest( 'td' ),
			$prevCell, $newTarget;

		if ( $currCell.length ) {
			$prevCell = $currCell.prev( 'td' );
			if ( ! $prevCell.length ) {
				$prevCell = $currCell.closest( 'tr' ).prev( 'tr' ).find( 'td' ).last();
			}
		}

		if ( $prevCell && $prevCell.length ) {
			$newTarget = $prevCell.find( '.ui-state-default' );
		}

		if ( $newTarget && $newTarget.length ) {
			highlightDate( inst, $newTarget );
		} else {
			goPreviousMonth( inst, target, 1 );
		}
	}

	function goNextDay( inst, target ) {
		var $currCell = $( target ).closest( 'td' ),
			$nextCell, $newTarget;

		if ( $currCell.length ) {
			$nextCell = $currCell.next( 'td' );
			if ( ! $nextCell.length ) {
				$nextCell = $currCell.closest( 'tr' ).next( 'tr' ).find( 'td' ).first();
			}
		}

		if ( $nextCell && $nextCell.length ) {
			$newTarget = $nextCell.find( '.ui-state-default' );
		}

		if ( $newTarget && $newTarget.length ) {
			highlightDate( inst, $newTarget );
		} else {
			goNextMonth( inst, target, 1 );
		}
	}

	function goFirstWeekDay( inst, target ) {
		var $currRow = $( target ).closest( 'tr' ),
			$newTarget = $currRow.find( '.ui-state-default' ).first();

		if ( $newTarget && $newTarget.length ) {
			highlightDate( inst, $newTarget );
		}
	}

	function goLastWeekDay( inst, target ) {
		var $currRow = $( target ).closest( 'tr' ),
			$newTarget = $currRow.find( '.ui-state-default' ).last();

		if ( $newTarget && $newTarget.length ) {
			highlightDate( inst, $newTarget );
		}
	}

	function goPreviousWeek( inst, target ) {
		var $currCell = $( target ).closest( 'td' ),
			$currRow = $currCell.closest( 'tr' ),
			$prevRow = $currRow.prev( 'tr' ),
			$prevCell = $prevRow.find( 'td' ).eq( $currCell.prevAll().length ),
			$newTarget;

		if ( $prevCell && $prevCell.length ) {
			$newTarget = $prevCell.find( '.ui-state-default' );
		}

		if ( $newTarget && $newTarget.length ) {
			highlightDate( inst, $newTarget );
		} else {
			goPreviousMonth( inst, target, 7 );
		}
	}

	function goNextWeek( inst, target ) {
		var $currCell = $( target ).closest( 'td' ),
			$currRow = $currCell.closest( 'tr' ),
			$nextRow = $currRow.next( 'tr' ),
			$nextCell = $nextRow.find( 'td' ).eq( $currCell.prevAll().length ),
			$newTarget;

		if ( $nextCell && $nextCell.length ) {
			$newTarget = $nextCell.find( '.ui-state-default' );
		}

		if ( $newTarget && $newTarget.length ) {
			highlightDate( inst, $newTarget );
		} else {
			goNextMonth( inst, target, 7 );
		}
	}

	function goPreviousMonth( inst, target, index ) {
		var $div = inst.dpDiv,
			date = $( target ).data( 'psDate' ),
			id = '#' + inst.id.replace( /\\\\/g, '\\' );

		if ( typeof index === 'undefined' ) {
			date = new Date( inst.drawYear, inst.drawMonth, +date );
		} else if ( typeof index === 'number' ) {
			date = new Date( inst.drawYear, inst.drawMonth, +date - index );
		}

		$.jQueryDatepicker._adjustDate( id, -1, 'M' );

		_.defer(function() {
			var $target;

			renderLabel( inst );

			$target = $div.find( 'a.ui-state-default' );
			$target = $target.filter( '[data-ps-date=' + date.getDate() + ']' );
			highlightDate( inst, $target );
		});
	}

	function goNextMonth( inst, target, index ) {
		var $div = inst.dpDiv,
			date = $( target ).data( 'psDate' ),
			id = '#' + inst.id.replace( /\\\\/g, '\\' );

		if ( typeof index === 'undefined' ) {
			date = new Date( inst.drawYear, inst.drawMonth, +date );
		} else if ( typeof index === 'number' ) {
			date = new Date( inst.drawYear, inst.drawMonth, +date + index );
		}

		$.jQueryDatepicker._adjustDate( id, 1, 'M' );

		_.defer(function() {
			var $target;

			renderLabel( inst );

			$target = $div.find( 'a.ui-state-default' );
			$target = $target.filter( '[data-ps-date=' + date.getDate() + ']' );
			highlightDate( inst, $target );
		});
	}

	function goPreviousYear( inst, target ) {
		var $div = inst.dpDiv,
			date = $( target ).data( 'psDate' ),
			id = '#' + inst.id.replace( /\\\\/g, '\\' );

		$.jQueryDatepicker._adjustDate( id, -12, 'M' );

		_.defer(function() {
			var $target;

			renderLabel( inst );

			$target = $div.find( 'a.ui-state-default' );
			$target = $target.filter( '[data-ps-date=' + date + ']' );
			highlightDate( inst, $target );
		});
	}

	function goNextYear( inst, target ) {
		var $div = inst.dpDiv,
			date = $( target ).data( 'psDate' ),
			id = '#' + inst.id.replace( /\\\\/g, '\\' );

		$.jQueryDatepicker._adjustDate( id, 12, 'M' );

		_.defer(function() {
			var $target;

			renderLabel( inst );

			$target = $div.find( 'a.ui-state-default' );
			$target = $target.filter( '[data-ps-date=' + date + ']' );
			highlightDate( inst, $target );
		});
	}

	function selectDate( inst, target ) {
		$( target ).click();
	}

	function closeDatepicker( inst ) {
		var $input = inst.input;
		$input.jQueryDatepicker( 'hide' );
	}

	function renderLabel( inst ) {
		var $div = inst.dpDiv,
			$table = $div.find( '.ui-datepicker-calendar' ),
			settings = inst.settings,
			days = settings.dayNames,
			month = settings.monthNames[ inst.drawMonth ],
			year = inst.drawYear;

		$table.attr( 'role', 'application' );
		$table.find( 'tbody a.ui-state-default' ).attr( 'role', 'button' ).each(function() {
			var $date = $( this ),
				index = $date.closest( 'td' ).prevAll().length,
				date = $.trim( $date.text() ),
				day = days[ index ],
				label;

			label = date + ', ' + day + ' ' + month + ' ' + year;
			$date.attr( 'aria-label', label );
			$date.attr( 'data-ps-date', date );
		});
	}

	function getCurrentDate( inst ) {
		var $div = inst.dpDiv,
			$current = $div.find( '.ui-state-highlight' );

		return $current;
	}

	function highlightDate( inst, target ) {
		var $div = inst.dpDiv,
			$current = getCurrentDate( inst ),
			$target = $( target );

		_.defer(function() {
			$current.removeClass( 'ui-state-highlight' );
			$target.focus();
			$target.addClass( 'ui-state-highlight' );
		});
	}

})( jQuery );
