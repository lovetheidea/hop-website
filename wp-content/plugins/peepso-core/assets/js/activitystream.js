(function( root, $, factory ) {

	var moduleName = 'PsActivityStream',
		moduleObject = factory( moduleName, $, peepso.observer );

	// Run on document load.
	jQuery(function( $ ) {
		var $container = $( '#ps-activitystream' ),
			config, inst;

		if ( $container.length ) {
			config = peepsodata && peepsodata.activity || {};
			inst = new moduleObject( $container[0], config );
		}
	});

})( window, jQuery, function( moduleName, $, observer ) {

	// Constants.
	var EVT_SCROLL = 'scroll.ps-activity-stream',
		MY_ID      = +peepsodata.currentuserid,
		USER_ID    = +peepsodata.userid,
		STREAM_ID  = $( '#peepso_stream_id' ).val() || undefined,
		POST_ID    = $( '#peepso_post_id' ).val() || undefined,
		CONTEXT    = $( '#peepso_context' ).val() || undefined;

	return peepso.createClass( moduleName, {

		/**
		 * Class constructor.
		 * @param {HTMLElement} container
		 */
		__constructor: function( container, config ) {
			this.$el = $( container );
			this.$loading = $( '#ps-activitystream-loading' );
			this.$noPosts = $( '#ps-no-posts' );
			this.$noMorePosts = $( '#ps-no-more-posts' );

			this.loading = false;
			this.loadEnd = false;
			this.loadPage = 1;
			this.loadLimit = +peepsodata.activity_limit_below_fold;
			this.loadButtonEnabled = +peepsodata.loadmore_enable;
			this.loadButtonTemplate = config.template_load_more;
			this.isPermalink = +config.is_permalink;
			this.hideFromGuest = +config.hide_from_guest;

			// Normalize load limit.
			this.loadLimit = this.loadLimit > 0 ? this.loadLimit : 3;

			// Hide loading if login popup is visible.
			$( document ).on( 'peepso_login_shown', function() {
				this.$loading.hide();
			});

			// Initiate infinite load.
			this.autoload();
		},

		/**
		 * Window scroll event handler.
		 */
		autoload: _.throttle(function() {
			var $button;

			if ( this.loading ) {
				return;
			}

			// Check if guest should be able to see the activities.
			if ( ! MY_ID && this.hideFromGuest ) {
				this.$loading.hide();
				return false;
			}

			if ( this.shouldLoad() ) {
				this.loading = true;
				this.load().done( $.proxy(function() {
					this.loading = false;
					if ( ! this.isPermalink && ! this.loadEnd ) {
						this.autoload();
					}
				}, this ) );

			} else if ( this.loadButtonEnabled ) {
				this.loadButtonEnabled = false;
				$button = $( this.loadButtonTemplate ).insertAfter( this.$el );
				$button.one( 'click', $.proxy(function( e ) {
					$( e.currentTarget ).remove();
					this.autoload();
				}, this ) );

			} else if ( ! this._onScrollEnabled ) {
				this._onScrollEnabled = true;
				$( window ).on( EVT_SCROLL, $.proxy( this.autoload, this ) );
			}
		}, 1000 ),

		/**
		 * Check whether stream should load next activities.
		 * @return {boolean}
		 */
		shouldLoad: function() {
			var $activities = this.$el.children( '.ps-js-activity' ),
				$last, limit, position, winHeight;

			// Do not try to load next activity if all activities is already loaded.
			if ( this.loadEnd ) {
				return false;
			}

			// Try to load next activities on empty stream.
			if ( ! $activities.length ) {
				return true;
			}

			// Get the first of the last N activities where N is decided by limit value.
			$last = $activities.slice( 0 - this.loadLimit ).eq( 0 );
			if ( ! $last.length ) {
				return false;
			}

			// Calculate element from viewport, or from top of the document if trigger button
			// is enabled.
			if ( this.loadButtonEnabled ) {
				position = $last.eq(0).offset();
			} else {
				position = $last[0].getBoundingClientRect();
			}

			// Load next activities if `$last` is still inside the viewport.
			winHeight = window.innerHeight || document.documentElement.clientHeight;
			if ( position.top < winHeight ) {
				return true;
			}

			return false;
		},

		/**
		 * Load next activities in the current stream.
		 * @return {jQuery.Deferred}
		 */
		load: function() {
			return $.Deferred( $.proxy(function( defer ) {
				var transport = peepso.disableAuth().disableError(),
					url = 'activity.show_posts_per_page',
					params;

				params = ps_observer.apply_filters('show_more_posts', {
					uid: MY_ID,
					user_id: USER_ID,
					stream_id: STREAM_ID,
					post_id: POST_ID,
					context: CONTEXT,
					page: this.loadPage,

					// Alo get pinned posts on first page.
					pinned: this.loadPage === 1 ? 1 : undefined
				});

				this.$loading.show();
				transport.postJson( url, params, $.proxy(function( json ) {
					var $posts;

					if ( json.data.found_posts > 0 ) {
						$posts = $( json.data.posts );

						// Show container if its not already visible.
						if ( this.$el.is( ':hidden' ) ) {
							this.$el.show();
						}

						// Safely append elements into container as some of them might raise error
						// when appended thus break the infinite load process.
						try {
							$posts.appendTo( this.$el );
						} catch( e ) {}

						$posts.hide().fadeIn( 1000, function() {
							$( document ).trigger( 'ps_activitystream_loaded' );
						});

						// TODO: This code should be moved to comments script.
						$( 'textarea[name=comment]', $posts ).ps_autosize();

						this.loadPage++;
					} else {
						this.loadEnd = true;
					}

					this.$loading.hide();

					if ( this.loadEnd ) {
						if ( params.page > 1 ) {
							this.$noMorePosts.show();
						} else {
							this.$noPosts.show();
						}
					}

					if ( ! $posts ) {
						$( document ).trigger( 'ps_activitystream_loaded' );
					}

					defer.resolve();

				}, this ) );
			}, this ) );
		}

	});

});
