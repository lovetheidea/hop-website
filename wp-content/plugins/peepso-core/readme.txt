=== PeepSo ===
Contributors: peepso, JaworskiMatt, rsusanto
Donate link: https://www.peepso.com/addons
Tags: community, social network, membership, user-profile, user-registration
Requires at least: 4.4
Tested up to: 4.9
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The Best Social Networking Plugin For WordPress. Beautiful user profiles, activity stream, member listing and many more.

== Description ==

PeepSo is a super-light, free, **social network** plugin for WordPress that allows you to quickly and effortlessly add a social network or an online community, similar to **Facebook** right inside your WordPress site. **Social network for WordPress** - in just a few clicks!
 
[youtube https://www.youtube.com/watch?v=2N7Y3kWj7A4]

The plugin supports the addition of a **social networking** plugin in the main area of your WordPress site. Widgets that display users' photos, videos and friends are also available on installation and can be placed on any widget position inside your theme, or directly inside [PeepSo](https://peepso.com/ "PeepSo - Social Network Plugin for WordPress") itself.

Our primary goal when developing this social networking plugin for WordPress was to provide those who wanted to create a **community** with WordPress an alternative to the existing solutions, with a modern look and feel, lots of great features and scalability.

What sets [PeepSo](https://peepso.com/ "PeepSo - Social Network Plugin for WordPress") apart from other **social networking** solutions is its light weight and stellar design. PeepSo works right out of the box and within seconds, you can start a **social network**, right inside your WordPress site. There's no need to hire a developer, mess with code, hack another plugin to make it look and work the way you want it. It simply works.

**Some of the features that PeepSo Core offers are:**

* Frontend user profiles with **avatars and cover images**.
* Frontend user registration
* Frontend user login
* Frontend user listing with search and filtering
* Customizable user profiles
* Activity Stream of the entire community
* Activity Stream for user profiles
* Users can share their status updates with **different privacy settings**
* **Real time onsite user notifications**
* User email notifications
* Dedicated user roles
* Nested Comments under user posts within your community
* Pinned Posts - these are great for making announcements
* PeepSo Profile widget with real time notifications
* PeepSo Online Members widget showing who’s online
* PeepSo Latest Members widget showing latest members
* Coherent configuration
* Dashboard with an Overview of your Community
* Light and Dark theme
* RTL support
* And many, many more...

Read about all of the features here: [PeepSo.com](https://www.peepso.com/foundation-plugins/)

**On top of all the previously mentioned features you'll also get:**

* **Stability** - it’s rock solid! You can sleep well and not worry about anything.
* Upgrades **will never break** anything on your site. 
* **Works out of the box** - 1st time. Everytime. Period.
* **Lots of Features** - and the Core is free. Forever.
* **Awesome Support** - yes. We proudly support our plugins. 


[PeepSo](https://wordpress.org/plugins/peepso-core/ "PeepSo - Social Network Plugin for WordPress") core is totally **free**. We encourage you to install it and see how it works. PeepSo has a range of extensions that allow you to extend the power of Your Community.

=== PeepSo Plugins ===

== FOUNDATION • THE FREE BASE OF YOUR COMMUNITY ==

* [Moods](https://wordpress.org/plugins/peepso-moods/) - Express your mood in posts
* [Tags](https://wordpress.org/plugins/peepso-tags/) - Mention others in posts and comments
* [Location](https://wordpress.org/plugins/peepso-location/) - Share location in posts and profiles


== --- Paid plugins for PeepSo --- ==

=== CORE • WHAT'S MOST IMPORTANT ===
* [Groups](https://www.peepso.com/downloads/groupso/) - Open, closed and secret user groups
* [Photos](https://www.peepso.com/downloads/picso) - Upload photos and create albums
* [Videos](https://www.peepso.com/downloads/vidso) - Link videos from supported providers
* [Polls](https://www.peepso.com/downloads/polls) - Post a question for others to vote
* [Extended Profiles](https://www.peepso.com/downloads/profileso) - Customizable profile fields
* [Friends](https://www.peepso.com/downloads/friendso) - Friend connections and "friends" privacy level
* [Chat](https://www.peepso.com/downloads/msgso) - Private messages and live chat between users
* [Reactions](https://www.peepso.com/downloads/reactions) - Say more than just a "like"


=== EXTRAS • EXTEND YOUR COMMUNITY EVEN FURTHER ===
* [BlogPosts](https://www.peepso.com/downloads/blogposts) - Integrate Blog within Community
* [Email Digest](https://www.peepso.com/downloads/emaildigest) - Bring users back with automated newsletter
* [User Limits](https://www.peepso.com/downloads/limitusers) - Granular control over user actions
* [VIP](https://www.peepso.com/downloads/vip) - Assign custom badges to users
* [WordFilter](https://www.peepso.com/downloads/wordfilter) - Filter out any kind of bad language
* [AutoFriends](https://www.peepso.com/downloads/autofriends) - Automatically create friend connections


=== INTEGRATIONS • COMBINE PEEPSO WITH OTHER SOLUTIONS ===
* [GIPHY](https://www.peepso.com/downloads/giphy) - Send GIPHY gifs in comments and chat
* [BadgeOS](https://www.peepso.com/downloads/peepso-badgeos) - Achievement system for community engagement
* [myCRED](https://www.peepso.com/downloads/peepso-mycred) - Points for community engagement


=== INTEGRATIONS - MONETIZATION • EARN MONEY FROM YOUR PEEPSO COMMUNITY
* [Advanced Ads](https://www.peepso.com/downloads/advads) - Social Targeted Ads Facebook Style
* [WPAdverts](https://www.peepso.com/downloads/wpadverts) - Monetize your community with paid ads
* [Paid Memberships Pro](https://www.peepso.com/downloads/peepso-pmp-integration) - Paid community memberships

=== THIRD PARTY • SOFTWARE BY OUR PARTNERS ===
There are also other plugins for PeepSo created by our Partners. You can see the full list of available plugins [here](https://PeepSo.com/addons/).


= Languages =

PeepSo and its plugins have been translated into the following languages:

* Brazilian Portuguese
* Bulgarian
* Chinese Traditional 
* Chinese Simplified
* Dutch
* English
* French
* German
* Greek 
* Hebrew
* Hungarian
* Italian
* Persian
* Polish
* Swedish
* Turkish
* And others

Here you can download the [Translations](https://www.peepso.com/translation/).
 
== Try the demo ==
 
If you want to see how a **social networking** on WordPress looks live, you can visit our [demo site](https://peepso.com/demo "PeepSo Demo"). You will be able to log in as a real user (username: demo, password: demo) and take PeepSo for a test drive. The demo includes all the premium plugins so you'll be able to see them all in action.

Or, simply join our own [Community](https://peepso.com/community "PeepSo Community") and see how it works in a real life environment :) 
 
== Shortcodes ==
 
PeepSo automatically generates pages for you with shortcodes to allow you to start fast. Here's a list of pages and shortcodes available. Use the shortcodes to display the content in case you've deleted the automatically generated page.
 
* User Profile - [peepso_profile]
* Recent Activity - [peepso_activity]
* Members - [peepso_members]
* Recover Password - [peepso_recover]
* Site Registration- [peepso_register]
 
== Getting Started ==
 
To get a social network on your WordPress site up and running, follow these steps:
 
* Login to your WordPress admin.
* Choose "Plugins".
* Search for "PeepSo".
* Click "Install".
* Go to "Appearance -> Menus"
* Select the menu you'd like to link to your social network.
* Add the page "Recent Activity" to that menu
* Save.
 
Your new **social network** will now be displayed on your site.

You can also start with PeepSo and [Divi](http://peep.so/et/) theme:

[youtube https://www.youtube.com/watch?v=SeKE7-kmMyw]

= More Information =

More plugins are currently being developed to extend PeepSo's functionality.

All of the plugins supporting the PeepSo core are available on [PeepSo.com](https://peepso.com/addons "PeepSo - Social Network Plugin for WordPress") 

== Installation ==

= From your WordPress dashboard =

1. Visit "Plugins > Add New"
2. Search for "PeepSo"
3. Press install
4. Activate PeepSo from your Plugins page.


= From WordPress.org =

1. Download PeepSo
2. Navigate to the Admin area of your site.
3. Go to Plugins > "Add New".
4. Select "Upload Plugin".
5. Select the ZIP installation file of PeepSo.
6. Activate PeepSo from your Plugins page.


== Frequently Asked Questions ==

= Can I use my existing theme? =

We've tested PeepSo with dozens of major themes. But since there are thousands of themes for WordPress, it'd hard for us to test with all of them. Well, virtually impossible. The themes we recommend are the [Divi](https://www.elegantthemes.com/gallery/divi/) and [Genesis](https://my.studiopress.com/themes/genesis/) and we're using those in testing / development on daily basis. 

You can check PeepSo core (it's free) with that theme and if you find any problems, please report them and we'll fix whatever PeepSo might be breaking. However, if the case is that the theme itself is interfering with PeepSo's styles and overriding them in any way, there's nothing we'd be able to do. You'd have to contact the theme developers to get that fixed.

= Where do I find translations? =

Here you can download the [Translations](https://www.peepso.com/addons/).

= Where do I get support for PeepSo? =

We encourage you to open a support ticket under [Your Account](https://www.peepso.com/my-account "PeepSo - Social Network Plugin for WordPress”). Our support operates Monday – Friday 9AM – 5PM (GMT+8). We reply to the majority of requests in less than 24 hours, but although we do our best it can take us up to 48 hours to get back to you (72 hours if the request was made just before or during the weekend).

= Is there documentation available for PeepSo? =

Yes! You can find documentation on the use of PeepSo and designing themes and extensions for PeepSo at [PeepSo Docs](https://docs.peepso.com "PeepSo - Social Network Plugin for WordPress")

= How do I report a bug/problem with PeepSo? =

We encourage you to open a support ticket under [Your Account](https://www.peepso.com/my-account "PeepSo - Social Network Plugin for WordPress”). Our support operates Monday – Friday 9AM – 5PM (GMT+8). We reply to the majority of requests in less than 24 hours, but although we do our best it can take us up to 48 hours to get back to you (72 hours if the request was made just before or during the weekend).

== Upgrade Notice ==

Upgrades can be performed in two ways. The automatic upgrade system lets you click a button on each plugin to upgrade to the latest available version. Alternatively, you can go to https://PeepSo.com/my-account/ and download the latest versions manually. Deactivate and delete the old plugins, upload, and activate new plugins.

For the automatic upgrades to work flawlessly make sure that you upgrade all the child plugins first and upgrade PeepSo Core last. You might need to reactivate the plugins too, once PeepSo Core is upgraded.

== Screenshots ==
1. 
2. 
3. 
4. 
5. 
6. 
7. 
8. 

== Changelog ==
= See the full changelog for every version =
* [PeepSo Changelog](https://www.peepso.com/changelog/ "PeepSo Changelog")
