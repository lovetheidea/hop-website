<form id="form_profile_deletion" class="ps-form--profile-deletion">
	<div class="ps-form__row">
		<div class="ps-form__controls">
			<input type="password" class="ps-input" width="auto" id="password-confirmation" name="password-confirmation" value="" />
			<label for="password-confirmation"><?php echo __('Password', 'peepso-core'); ?></label>
		</div>
	</div>
</form>