<?php
$PeepSoUser = PeepSoUser::get_instance(PeepSoProfileShortcode::get_instance()->get_view_user_id());
if( get_current_user_id() == $PeepSoUser->get_id()) {

    $tab = isset($tab) ? $tab : FALSE;
    ?>
    <div class="ps-tabs__wrapper">
        <div class="ps-tabs ps-tabs--arrows">
            <div class="ps-tabs__item <?php if (!strlen($tab)) echo "current"; ?>"><a
                        href="<?php echo $PeepSoUser->get_profileurl() . 'about/'; ?>"><?php _e('About', 'peepso-core'); ?></a>
            </div>

            <div class="ps-tabs__item <?php if ('preferences' == $tab) echo "current"; ?>"><a
                        href="<?php echo $PeepSoUser->get_profileurl() . 'about/preferences/'; ?>"><?php _e('Preferences', 'peepso-core'); ?></a>
            </div>

            <div class="ps-tabs__item <?php if ('account' == $tab) echo "current"; ?>"><a
                        href="<?php echo $PeepSoUser->get_profileurl() . 'about/account/'; ?>"><?php _e('Account', 'peepso-core'); ?></a>
            </div>
        </div>
    </div>

    <?php
}