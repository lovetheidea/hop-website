<?php
$user = PeepSoUser::get_instance(PeepSoProfileShortcode::get_instance()->get_view_user_id());

$can_edit = FALSE;
if($user->get_id() == get_current_user_id() || current_user_can('edit_users')) {
    $can_edit = TRUE;
}

if(!$can_edit) {
    PeepSo::redirect($user->get_profileurl());
} else {

    $PeepSoProfile = PeepSoProfile::get_instance();
    ?>

    <div class="peepso ps-page-profile">
        <?php PeepSoTemplate::exec_template('general', 'navbar'); ?>

        <?php PeepSoTemplate::exec_template('profile', 'focus', array('current'=>'about')); ?>

        <section id="mainbody" class="ps-page-unstyled">
            <section id="component" role="article" class="clearfix">


                <?php if($can_edit) { PeepSoTemplate::exec_template('profile', 'profile-about-tabs', array('tab'=>'account'));} ?>

                <div class="ps-list--column cfield-list creset-list ps-js-profile-list">

                    <div class="ps-form-container">
                        <?php if (strlen($PeepSoProfile->edit_form_message())) { ?>
                            <div class="ps-alert ps-alert-success">
                                <?php echo $PeepSoProfile->edit_form_message(); ?>
                            </div>
                        <?php } ?>
                        <div class="ps-form-legend">
                            <?php _e('Basic Information', 'peepso-core'); ?>
                        </div>
                        <?php $PeepSoProfile->edit_form(); ?>
                        <div class="ps-form-group">
                            <label for=""></label>
                            <span class="ps-form-helper"><?php _e('Fields marked with an asterisk (<span class="required-sign">*</span>) are required.', 'peepso-core'); ?></span>
                        </div>
                    </div> <!-- .clayout -->

                </div>
            </section><!--end component-->
        </section><!--end mainbody-->
    </div><!--end row-->
<?php }