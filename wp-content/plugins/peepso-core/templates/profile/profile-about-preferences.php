<?php
$user = PeepSoUser::get_instance(PeepSoProfileShortcode::get_instance()->get_view_user_id());

$can_edit = FALSE;
if($user->get_id() == get_current_user_id() || current_user_can('edit_users')) {
    $can_edit = TRUE;
}

if(!$can_edit) {
    PeepSo::redirect($user->get_profileurl());
} else {

    $PeepSoProfile = PeepSoProfile::get_instance();
    ?>

    <div class="peepso ps-page-profile ps-page--preferences">
        <?php PeepSoTemplate::exec_template('general', 'navbar'); ?>

        <?php PeepSoTemplate::exec_template('profile', 'focus', array('current'=>'about')); ?>

        <section id="mainbody" class="ps-page-unstyled">
            <section id="component" role="article" class="clearfix">


                <?php if($can_edit) { PeepSoTemplate::exec_template('profile', 'profile-about-tabs', array('tab'=>'preferences'));} ?>


                <div class="ps-list--column cfield-list creset-list ps-js-profile-list">

                    <div class="cfield-list creset-list">
                        <?php if ($PeepSoProfile->num_preferences_fields()) { ?>
                            <?php $PeepSoProfile->preferences_form_fields(); ?>
                        <?php } else { ?>
                            <?php _e('You have no configurable preferences settings.', 'peepso-core'); ?>
                        <?php } ?>
                    </div>

                </div>
            </section><!--end component-->
        </section><!--end mainbody-->
    </div><!--end row-->
<?php }