<?php
/**
 * Created by Love the Idea.
 * Edited: Jason Pickering
 * Date: 13/10/17
 * Time: 18:30
 */

/** Requiere the JWT library. */
use \Firebase\JWT\JWT;

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://enriquechavez.co
 * @since      1.0.0
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @author     Enrique Chavez <noone@tmeister.net>
 */

header("Access-Control-Allow-Origin: *");

class Jwt_Auth_Public
{
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 *
	 * @var string The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 *
	 * @var string The current version of this plugin.
	 */
	private $version;

	/**
	 * The namespace to add to the api calls.
	 *
	 * @var string The namespace to add to the api call
	 */
	private $namespace;

	/**
	 * Store errors to display if the JWT is wrong
	 *
	 * @var WP_Error
	 */
	private $jwt_error = null;

	/**
	 * Store errors to display if the JWT is wrong
	 *
	 * @var WP_Error
	 */
	private $metaWhiteList = array();



	private $salt;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version     The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->namespace = $this->plugin_name.'/v'.intval($this->version);
		$this->salt = '039270973072hk09h2389294h928hg982h@@£@%g34B££$FHK^&+=45£@£sFD$^EGDFHY^$ESHET%$%@@@FBnnnNN<<,,n,n,eHETE';
		$this->metaWhiteList = array(
			'email_invites',
			'phone_invites',
			'facebook_invites',
			'hop_loyalty_name',
			'hop_loyalty_email',
			'hop_loyalty_card',
			'hop_loyalty_promotion',
			'payment_method',
			'payment_name',
			'payment_card_no',
			'payment_month',
			'payment_year',
			'payment_cvv',
			'payment_auto_reload',
			'billing_name',
			'billing_address_1',
			'billing_address_2',
			'billing_city',
			'billing_state',
			'billing_zip_post',
			'hop_current_fund',
			'hop_points',
			'new_promotions',
			'push_token',
			'email_account_alerts',
			'badges'
		);
		$this->extraWhiteList = array(
		   'cutinline' => 'Cut In Line ',
		   'reservations' => 'Reservations',
		   'valet' => 'Valet Available'
		 );
		 $this->eventWhiteList = array(
		  'events' => 'job_events',
		  'contests' => 'job_contest',
		  'freeFun' => 'job_fun',
		 );
	}

	/**
	 * Add the endpoints to the API
	 */
	public function add_api_routes()
	{
		register_rest_route($this->namespace, 'token', [
			'methods' => 'POST',
			'callback' => array($this, 'generate_token'),
		]);

		register_rest_route($this->namespace, 'check', [
			'methods' => 'POST',
			'callback' => array($this, 'generate_account'),
		]);

		register_rest_route($this->namespace, 'profile', [
    			'methods' => 'POST',
    			'callback' => array($this, 'get_account'),
    		]);

		register_rest_route($this->namespace, 'userMeta', [
			'methods' => 'POST',
			'callback' => array($this, 'add_user_meta'),
		]);

		register_rest_route($this->namespace, 'getUsers', [
      'methods' => 'GET, OPTIONS',
      'callback' => array($this, 'get_user'),
    ]);

    register_rest_route($this->namespace, 'addUser', [
      'methods' => 'GET, OPTIONS',
      'callback' => array($this, 'add_friend'),
    ]);

		register_rest_route($this->namespace, 'createGroup', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'create_group'),
    ]);

    register_rest_route($this->namespace, 'createPost', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'create_post'),
    ]);

    register_rest_route($this->namespace, 'createCheckin', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'create_checkin'),
    ]);


    register_rest_route($this->namespace, 'createMessage', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'create_message'),
    ]);

     register_rest_route($this->namespace, 'checkMessage', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'check_message'),
    ]);

    register_rest_route($this->namespace, 'getListings', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'get_listings'),
    ]);

    register_rest_route($this->namespace, 'getOrders', [
      'methods' => 'GET, OPTIONS',
      'callback' => array($this, 'get_orders'),
    ]);

    register_rest_route($this->namespace, 'checkOrderHash', [
      'methods' => 'POST, OPTIONS',
      'callback' => array($this, 'check_order_hash'),
    ]);


    register_rest_route($this->namespace, 'createOrder', [
          'methods' => 'POST, OPTIONS',
          'callback' => array($this, 'create_order'),
        ]);

    register_rest_route($this->namespace, 'getProducts', [
      'methods' => 'GET, OPTIONS',
      'callback' => array($this, 'get_products'),
    ]);

		register_rest_route($this->namespace, 'uploadProfile', [
			'methods' => 'POST, OPTIONS',
			'callback' => array($this, 'update_profile'),
		]);

		register_rest_route($this->namespace, 'token/validate', array(
			'methods' => 'POST',
			'callback' => array($this, 'validate_token'),
		));
	}

	/**
	 * Add CORs support to the request.
	 */
	public function add_cors_support() {
		$enable_cors = defined('JWT_AUTH_CORS_ENABLE') ? JWT_AUTH_CORS_ENABLE : false;
		if ($enable_cors) {
			$headers = apply_filters('jwt_auth_cors_allow_headers', 'Access-Control-Allow-Headers, Content-Type, Authorization');
			header(sprintf('Access-Control-Allow-Headers: %s', $headers));
		}
	}

	private function user_id_exists($user){

      global $wpdb;

      $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE ID = %d", $user));

      if($count == 1){ return true; }else{ return false; }

  }

	public function get_user($request) {

    $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
    $user_id = $request->get_param('id') ? $request->get_param('id') : $request->get_param('author');
    $search = $request->get_param('search');

    if ($user_id && !is_numeric( $user_id )) {
      return new WP_Error(
        'jwt_auth_bad_config: E001',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }
    /** First thing, check the secret key if not exist return a error*/
    if (!$secret_key) {
      return new WP_Error(
        'jwt_auth_bad_config: E002',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }

    $users = [];
    if(user_id && !$search){
      $blogusers = get_users( array( 'meta_key' => 'friends', 'meta_value' => $user_id.'"', 'meta_compare' => 'LIKE' ) );
      $pendingusers = get_users( array( 'meta_key' => 'pending_friends', 'meta_value' => $user_id.'"', 'meta_compare' => 'LIKE' ) );
      $sentpendingusers = get_users( array( 'meta_key' => 'sent_pending_friends', 'meta_value' => $user_id.'"', 'meta_compare' => 'LIKE' ) );
      foreach ( $sentpendingusers as $user ) {
        $u = [
          'name' => ucwords($user->display_name) ,
          'id' => $user->ID,
          'pending' => true
        ];
        if (strlen(wp_user_avatars_get_local_avatar_url($user->ID, 480)) > 0) {
           $u['avatar']= wp_user_avatars_get_local_avatar_url($user->ID, 480);
        }else {
          $u['avatar'] = get_avatar_url($user->ID);
        }
        $users[] = $u;
      }
      foreach ( $pendingusers as $user ) {
        $u = [
          'name' => ucwords($user->display_name) ,
          'id' => $user->ID,
          'sent' => true
        ];
        if (strlen(wp_user_avatars_get_local_avatar_url($user->ID, 480)) > 0) {
           $u['avatar']= wp_user_avatars_get_local_avatar_url($user->ID, 480);
        }else {
          $u['avatar'] = get_avatar_url($user->ID);
        }
        $users[] = $u;
      }

    } else {
      $searchExpand = !empty($search) && strlen($search) > 2 ? $search."*" : $search.'a';
	    $blogusers = $search ? get_users( array( 'search' => $searchExpand ) ) :  get_users( array( 'fields' => array( 'display_name','first_name','last_name', 'ID' ) ) );
	  }
    // Array of WP_User objects.

    foreach ( $blogusers as $user ) {
        $u = [
          'name' => ucwords($user->display_name) ,
          'id' => $user->ID,
        ];
        if (strlen(wp_user_avatars_get_local_avatar_url($user->ID, 480)) > 0) {
           $u['avatar']= wp_user_avatars_get_local_avatar_url($user->ID, 480);
        }else {
          $u['avatar'] = get_avatar_url($user->ID);
        }
        $users[] = $u;
    }

    return wp_send_json($users);
	}

	public function add_friend($request) {

    $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
    $user_id = $request->get_param('id') ? $request->get_param('id') : $request->get_param('author');
    $friend_id = $request->get_param('friend');
    $pending_friend_id = $request->get_param('pending');
    $restaurant = $request->get_param('restaurant') ? $request->get_param('restaurant') : $request->get_param('post');

    $friendTrue = self::user_id_exists($friend_id);

    if (!$user_id || !is_numeric( $user_id ) || ($friend_id && !$friendTrue) || (!$friend_id && !$restaurant && !$pending_friend_id)) {
      return new WP_Error(
        'jwt_auth_bad_config: E001',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }

    if($pending_friend_id){

      //checkfriends
      if($check1 = get_user_meta($user_id, 'friends', true)){
        if (($key = array_search($friend_id, $check1)) !== false) {
            return [];
        }
      }

      $myFriends = is_array(get_user_meta($user_id, 'sent_pending_friends', true)) ? get_user_meta($user_id, 'sent_pending_friends', true) : [];
      $myFriends[] = $pending_friend_id;
      update_user_meta($user_id, 'sent_pending_friends', array_unique($myFriends));

      $thereFriends = is_array(get_user_meta($pending_friend_id, 'pending_friends', true)) ? get_user_meta($pending_friend_id, 'pending_friends', true) : [];
      $thereFriends[] = $user_id;
      update_user_meta($pending_friend_id, 'pending_friends', array_unique($thereFriends));


      //TODO TRIGGER EMAIL.

    } else if($friend_id) {

      $myFriends = is_array(get_user_meta($user_id, 'friends', true)) ?  get_user_meta($user_id, 'friends', true) : [];
      $myFriends[] = $friend_id;
      update_user_meta($user_id, 'friends', array_unique($myFriends));

      $thereFriends = is_array(get_user_meta($friend_id, 'friends', true)) ? get_user_meta($friend_id, 'friends', true) : [];
      $thereFriends[] = $user_id;
      update_user_meta($friend_id, 'friends', array_unique($thereFriends));


      // remove pending (if any)

      $myPFriends = is_array(get_user_meta($user_id, 'pending_friends', true)) ? get_user_meta($user_id, 'pending_friends', true) : [];
      if (($key = array_search($friend_id, $myPFriends)) !== false) {
          unset($myPFriends[$key]);
      }
      update_user_meta($user_id, 'pending_friends', array_unique($myPFriends));

      $therePFriends = is_array(get_user_meta($friend_id, 'pending_friends', true)) ? get_user_meta($friend_id, 'pending_friends', true) : [];
      if (($key = array_search($user_id, $therePFriends)) !== false) {
          unset($therePFriends[$key]);
      }
      update_user_meta($friend_id, 'pending_friends', array_unique($therePFriends));


      $mySFriends = is_array(get_user_meta($user_id, 'sent_pending_friends', true)) ? get_user_meta($user_id, 'sent_pending_friends', true) : [];
      if (($key = array_search($friend_id, $mySFriends)) !== false) {
          unset($mySFriends[$key]);
      }
      update_user_meta($user_id, 'sent_pending_friends', array_unique($mySFriends));

      $thereSFriends = is_array(get_user_meta($friend_id, 'sent_pending_friends', true)) ? get_user_meta($friend_id, 'sent_pending_friends', true) : [];
      if (($key = array_search($user_id, $thereSFriends)) !== false) {
          unset($thereSFriends[$key]);
      }
      update_user_meta($friend_id, 'sent_pending_friends', array_unique($thereSFriends));

      //TODO TRIGGER EMAIL.

    } else {

       $myFriends = is_array(get_post_meta($restaurant, 'friends', true)) ?  get_post_meta($restaurant, 'friends', true) : [];
       $myFriends[] = $user_id;
       update_post_meta($restaurant, 'friends', array_unique($myFriends));

    }

    return ['success' => true];


  }

	public function returnUserObj($user = null, $token = false){
		global $blog_id, $wpdb;
		if(!$user){
			return [];
		}
		/** The token is signed, now create the object with no sensible user data to the client*/
		$data = array(
			'user_email' => $user->data->user_email,
			'user_nicename' => $user->data->user_nicename,
			'user_display_name' => $user->data->display_name,
			'user_id' => $user->data->ID,
		);

		foreach($this->metaWhiteList as $key){
			$m = get_user_meta($user->data->ID, $key, true);
			//if ($m !== false && strlen($m) > 0){
				$data[$key] = $m;
			//}
		}
		//avatar
		if (strlen(wp_user_avatars_get_local_avatar_url($user->data->ID, 480)) > 0) {
			 $data['user_avatar']= wp_user_avatars_get_local_avatar_url($user->data->ID, 480);
		}
		//token
		if($token){
			$data['token'] = $token;
		}
		return $data;
	}

	public function upload_image_from_base64($filename, $img_base64Str = [], $post_id = 0, $user_id = 1, $passContent ='' ){

		global $blog_id, $wpdb;
		if(!empty($img_base64Str)) {

			list($type, $img_base64Str) = explode(';', $img_base64Str);
			list(, $type)        = explode(':', $type);
			list(, $type)        = explode('/', $type);
			list(, $img_base64Str)      = explode(',', $img_base64Str);

			$type = strtolower($type);

			$img = base64_decode($img_base64Str);

			/*
			wp_upload_dir( null, false );
			 */
			$wp_upload_dir = wp_upload_dir();
			$filename = md5($filename).time().".$type";
			$path_to_file = $wp_upload_dir['path']."/".$filename;
			$filetype = wp_check_filetype( basename( $filename), null );

			if( !function_exists( 'wp_handle_upload' ) ){
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
			}

			@file_put_contents( $path_to_file, $img );

			$attachment = array(
				'post_author' => $user_id,
				'post_content' => $passContent,
				'post_content_filtered' => '',
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_excerpt' => '',
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'post_mime_type' => $filetype['type'],
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				'post_password' => '',
				'to_ping' =>  '',
				'pinged' => '',
				'post_parent' => $post_id,
				'menu_order' => 0,
				'guid' => $wp_upload_dir['url'].'/'.$filename,
			);
			$wpdb->insert("{$wpdb->prefix}posts", $attachment);
			$attach_id = $wpdb->insert_id;
			if($attach_id == false){
				return false;
			}

			$attach_data = wp_generate_attachment_metadata( $attach_id, $path_to_file );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			update_attached_file($attach_id, $path_to_file);

			return $attach_id;
		}
		return false;


	}

	/**
	 * Add Usermeta to User Object.
	 *
	 * @param [JSON] $request [{id:user_id, meta:[key=>val]}]
	 *
	 * @return [type] [description]
	 */
	public function add_user_meta($request)
	{

		$secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
		$user_id = $request->get_param('id');
		$meta = $request->get_param('meta');

		if (is_numeric( $user_id ) && !empty($meta)) {
			$user_id = $user_id;
		} else {
			return new WP_Error(
				'jwt_auth_bad_config',
				__('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}
    /** First thing, check the secret key if not exist return a error*/
    if (!$secret_key) {
      return new WP_Error(
        'jwt_auth_bad_config',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }

		foreach($meta as $meta_key => $meta_val){
			if (!in_array($meta_key, $this->metaWhiteList)) {
				$secret_key = false;
			}
			/** add user meta */
			update_user_meta( $user_id, $meta_key, $meta_val);
		}

		return self::returnUserObj(get_userdata($user_id));
	}
	/**
		 * Get the user and password in the request body and generate a JWT
		 *
		 * @param [type] $request [description]
		 *
		 * @return [type] [description]
		 */

	public function generate_token($request)
	{
		$secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
		$username = $request->get_param('username');
		$password = $request->get_param('password');

		/** First thing, check the secret key if not exist return a error*/
		if (!$secret_key) {
			return new WP_Error(
				'jwt_auth_bad_config',
				__('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}
		/** Try to authenticate the user with the passed credentials*/
		$user = wp_authenticate($username, $password);

		/** If the authentication fails return a error*/
		if (is_wp_error($user)) {
			$error_code = $user->get_error_code();
			return new WP_Error(
				'[jwt_auth] '.$error_code,
				$user->get_error_message($error_code),
				array(
					'status' => 403,
				)
			);
		}

		/** Valid credentials, the user exists create the according Token */
		$issuedAt = time();
		$notBefore = apply_filters('jwt_auth_not_before', $issuedAt, $issuedAt);
		$expire = apply_filters('jwt_auth_expire', $issuedAt + (DAY_IN_SECONDS * 7), $issuedAt);

		$token = array(
			'iss' => get_bloginfo('url'),
			'iat' => $issuedAt,
			'nbf' => $notBefore,
			'exp' => $expire,
			'data' => array(
				'user' => array(
					'id' => $user->data->ID,
				),
			),
		);

		/** Let the user modify the token data before the sign. */
		$token = JWT::encode(apply_filters('jwt_auth_token_before_sign', $token, $user), $secret_key);

		/** The token is signed, now create the object with no sensible user data to the client*/
		$data = self::returnUserObj($user, $token);

		//TODO TRIGGER EMAIL

		/** Let the user modify the data before send it back */
		return apply_filters('jwt_auth_token_before_dispatch', $data, $user);
	}


	public function get_account($request) {

	    $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
  		$id = $request->get_param('id');

  		/** First thing, check the secret key if not exist return a error*/
  		if (!$secret_key || $id == false || $id == null) {
  			return new WP_Error(
  				'jwt_auth_bad_config',
  				__('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
  				array(
  					'status' => 403,
  				)
  			);
  		}

  		$error = false;
  		/** Try to authenticate the user credentials*/
  		$user_id = intval($id);
  		if($user_id != false) {
  			$user = get_userdata($user_id);
  		}
  		else {
  			$error = true;
  		}

  		/** If the authentication fails return a error*/
  		if ($error || is_wp_error( $user )) {
  			$error_code = $user->get_error_code();
  			return new WP_Error(
  				'[jwt_auth] '.$error_code,
  				$user->get_error_message($error_code),
  				array(
  					'status' => 403,
  				)
  			);
  		}

  		$data = self::returnUserObj($user);

  		/** Let the user modify the data before send it back */
  		return $data;

	}

	/**
	 * Create a user if not already in the database
	 *
	 * @param [JSON] $request [{ email: email, name: name}]
	 *
	 * @return [JSON] [description]
	 */
	public function generate_account($request) {
		$secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
		$email = $username = $request->get_param('email');
		$name = $request->get_param('name');

		/** First thing, check the secret key if not exist return a error*/
		if (!$secret_key || $email == false || $email == null) {
			return new WP_Error(
				'jwt_auth_bad_config',
				__('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}
		/** Try to authenticate the user credentials*/
		$user_id = email_exists($email);
		if($user_id != false) {
			wp_update_user( array( 'ID' => $user_id, 'user_nicename' => $name, 'display_name' => $name ) );
			$user = get_userdata($user_id);
		}
		else {
			$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			$user_id = wp_create_user( $email, $random_password, $email );
			wp_update_user( array( 'ID' => $user_id, 'user_nicename' => $name, 'display_name' => $name ) );
			$user = get_userdata($user_id);
		}

		/** If the authentication fails return a error*/
		if (is_wp_error( $user )) {
			$error_code = $user->get_error_code();
			return new WP_Error(
				'[jwt_auth] '.$error_code,
				$user->get_error_message($error_code),
				array(
					'status' => 403,
				)
			);
		}

		$data = self::returnUserObj($user);

		/** Let the user modify the data before send it back */
		return $data;
	}

	/**
	 * Create a user if not already in the database
	 *
	 * @param [JSON] $request [{ email: email, name: name}]
	 *
	 * @return [JSON] [description]
	 */
	public function update_profile($request)
	{
		if($request->get_method() == "OPTIONS"){
			return array("result" => "Pre-flight checked");
		}

		$secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
		$json = $request->get_json_params();
		$user_id = $email = $json['user_id'];
		$valid = $json['valid'];
		$file_name = $json['filename'];
		$file_data = $json['data'];

		/** First thing, check the secret key if not exist return a error*/
		if (!$secret_key || $user_id == false || $user_id == null) {
			return new WP_Error(
				'jwt_auth_bad_config',
				__('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}
		/** Try to authenticate the user credentials*/
		if(is_string( $user_id )){
			$user_id =  email_exists($user_id);
		} else if (is_numeric( $user_id )) {
			$user_id = $user_id;
		}
		if($user_id != false) {
			$user = get_userdata($user_id);
		}

		try{
			$attachment_id = self::upload_image_from_base64($file_name, $file_data, 0, $user_id);
		}catch (ErrorException $err){
			return new WP_Error(
				'Cannot attach',
				__($err, 'cannot-attach-err'),
				array(
					'status' => 403,
				)
			);
		}

		// ensure the media is real is an image
		if ( wp_attachment_is_image( $attachment_id ) ) {
			wp_user_avatars_update_avatar( $user_id, $attachment_id );
		} else{
			return new WP_Error(
				'Cannot attach',
				__('Cannot attach file', 'cannot-attach'),
				array(
					'status' => 403,
				)
			);
		}

		//TODO TRIGGER EMAIL

		/** The token is signed, now create the object with no sensible user data to the client*/
		$data = self::returnUserObj($user, $attachment_id);
		/** Let the user modify the data before send it back */
		return $data;
	}



  /**
  * Create a group if not already in the database
  *
  * @param [JSON] $request [{ email: email, name: name}]
  *
  * @return [JSON] [description]
  */
  public function create_group($request){
    if($request->get_method() == "OPTIONS"){
      return array("result" => "Pre-flight checked");
    }

    $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
    $json = $request->get_json_params();
    $user_id = $email = $json['user_id'];
    $title = $json['title'];
    $desc = $json['description'];
    $status = $json['status'];
    $valid = $json['valid'];
    $file_name = $json['filename'];
    $file_data = $json['data'];

    /** First thing, check the secret key if not exist return a error*/
    if (!$secret_key || $user_id == false || $user_id == null) {
      return new WP_Error(
        'jwt_auth_bad_config',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }

    /** Try to authenticate the user credentials*/
    if(is_string( $user_id )){
      $user_id =  email_exists($user_id);
    } else if (is_numeric( $user_id )) {
      $user_id = $user_id;
    }
    if($user_id != false) {
      $user = get_userdata($user_id);
    }

    // Initialize the page ID to -1. This indicates no action has been taken.
    $post_id = -1;
    $slug = sanitize_title_with_dashes($title);

    // If the page doesn't already exist, then create it
    if( null == get_page_by_title( $title ) ) {
      // Set the post ID so that we know the post was created successfully
      $post_id = wp_insert_post(
        array(
          'comment_status'	=>	'open',
          'ping_status'		=>	'closed',
          'post_author'		=>	$user_id,
          'post_name'		=>	$slug,
          'post_title'		=>	$title,
          'post_content'		=>	$desc,
          'post_category'		=>	($status == 'private' ? [114]: [2]),
          'post_status'		=>	'publish',
          'post_type'		=>	'group'
        )
      );

      if($file_name && $file_data) {
        try{
          $attachment_id = self::upload_image_from_base64($file_name, $file_data, $post_id);
        }catch (ErrorException $err){
          return new WP_Error(
            'Cannot attach, image_from_base64',
            __($err, 'cannot-attach-err'),
            array(
              'status' => 403,
            )
          );
        }

        // ensure the media is real is an image
        if ( wp_attachment_is_image( $attachment_id ) ) {
          set_post_thumbnail( $post_id, $attachment_id );
        } else{
          return new WP_Error(
            'Cannot attach',
            __('Cannot attach file', 'cannot-attach'),
            array(
              'status' => 403,
            )
          );
        }
      }
    } else {
      return new WP_Error(
        'Group name already taken',
        __('Cannot creat a group', 'title-taken'),
        array(
          'status' => 403,
        )
      );
    } // end if

    return ['group_id' => $post_id];
  }



  /**
    * Create a post if not already in the database
    *
    * @param [JSON] $request [{ email: email, name: name}]
    *
    * @return [JSON] [description]
    */
    public function create_post($request){
      if($request->get_method() == "OPTIONS"){
        return array("result" => "Pre-flight checked");
      }

      $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
      $json = $request->get_json_params();
      $user_id = $email = $json['user_id'];
      $title = $json['title'];
      $desc = $json['content'];
      $status = $json['status'];
      $valid = $json['valid'];
      $file_name = $json['filename'];
      $file_data = $json['data'];

      /** First thing, check the secret key if not exist return a error*/
      if (!$secret_key || $user_id == false || $user_id == null) {
        return new WP_Error(
          'jwt_auth_bad_config',
          __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
          array(
            'status' => 403,
          )
        );
      }

      /** Try to authenticate the user credentials*/
      if(is_string( $user_id )){
        $user_id =  email_exists($user_id);
      } else if (is_numeric( $user_id )) {
        $user_id = $user_id;
      }
      if($user_id != false) {
        $user = get_userdata($user_id);
      }

      // Initialize the page ID to -1. This indicates no action has been taken.
      $post_id = -1;
      $slug = sanitize_title_with_dashes($title);

      // If the page doesn't already exist, then create it
      if( null == get_page_by_title( $title ) ) {
        // Set the post ID so that we know the post was created successfully
        $post_id = wp_insert_post(
          array(
            'comment_status'	=>	'open',
            'ping_status'		=>	'closed',
            'post_author'		=>	$user_id,
            'post_name'		=>	$slug,
            'post_title'		=>	$title,
            'post_content'		=>	$desc,
            'post_category'		=>	($status == 'private' ? [114]: [2]),
            'post_status'		=>	'publish',
            'post_type'		=>	'post'
          )
        );

        if($file_name && $file_data) {
          try{
            $attachment_id = self::upload_image_from_base64($file_name, $file_data, $post_id, $desc);
          }catch (ErrorException $err){
            return new WP_Error(
              'Cannot attach, image_from_base64',
              __($err, 'cannot-attach-err'),
              array(
                'status' => 403,
              )
            );
          }

          // ensure the media is real is an image
          if ( wp_attachment_is_image( $attachment_id ) ) {
            set_post_thumbnail( $post_id, $attachment_id );
          } else{
            return new WP_Error(
              'Cannot attach',
              __('Cannot attach file', 'cannot-attach'),
              array(
                'status' => 403,
              )
            );
          }
        }
      } else {
        return new WP_Error(
          'Group name already taken',
          __('Cannot creat a group', 'title-taken'),
          array(
            'status' => 403,
          )
        );
      } // end if

      return ['post_id' => $post_id];
    }



    /**
    * Create a checkin if not already in the database
    *
    * @param [JSON] $request [{ email: email, name: name}]
    *
    * @return [JSON] [description]
    */
    public function create_checkin($request){
      if($request->get_method() == "OPTIONS"){
        return array("result" => "Pre-flight checked");
      }

      $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
      $json = $request->get_json_params();
      $user_id = $email = $json['user_id'];
      $longLat = $json['longLat'];
      $status = $json['status'];
      $restaurant = intval($json['restaurant']);

      /** First thing, check the secret key if not exist return a error*/
      if (!$secret_key || $user_id == false || $user_id == null) {
        return new WP_Error(
          'jwt_auth_bad_config',
          __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
          array(
            'status' => 403,
          )
        );
      }

      /** Try to authenticate the user credentials*/
      if(is_string( $user_id )){
        $user_id =  email_exists($user_id);
      } else if (is_numeric( $user_id )) {
        $user_id = $user_id;
      }
      if($user_id != false) {
        $user = get_userdata($user_id);
      }

      $rest = get_post($restaurant);
      $title = $user->first_name.' just checked into '. $rest->title;

      // Initialize the page ID to -1. This indicates no action has been taken.
      $post_id = -1;
      $slug = sanitize_title_with_dashes($title);

      // If the page doesn't already exist, then create it
      if( null == get_page_by_title( $title ) ) {
        // Set the post ID so that we know the post was created successfully
        $post_id = wp_insert_post(
          array(
            'comment_status'	=>	'open',
            'ping_status'		=>	'closed',
            'post_author'		=>	$user_id,
            'post_name'		=>	$slug,
            'post_title'		=>	$title,
            'post_category'		=>	($status == 'private' ? [114]: [2]),
            'post_status'		=>	'publish',
            'post_type'		=>	'checkin'
          )
        );

        /** add user meta */
        add_post_meta( $post_id, 'restaurant', $restaurant);
        add_post_meta( $post_id, 'long_lat', $longLat);

      } else {
        return new WP_Error(
          'Already checked in!',
          __('Cannot creat a group', 'title-taken'),
          array(
            'status' => 403,
          )
        );
      } // end if

      return ['checkin_id' => $post_id];
    }


  public function check_message($request){
      if($request->get_method() == "OPTIONS"){
        return array("result" => "Pre-flight checked");
      }
      $json = $request->get_json_params();
      $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
      $user_id = $json['user_id'];
      $friend_id  = ($json['to']);
      $title = md5($user_id.$friend_id.'897sg8sd8g9sdg9');
      $title2 = md5($friend_id.$user_id.'897sg8sd8g9sdg9');

      /** First thing, check the secret key if not exist return a error*/
      if (!$secret_key || $friend_id == null || $user_id == null) {
        return new WP_Error(
          'jwt_auth_bad_config',
          __('JWT key is not configurated properly, please contact the admin:'. $friend_id.$user_id, 'wp-api-jwt-auth'),
          array(
            'status' => 403,
          )
        );
      }

      /** Try to authenticate the user credentials*/
      if(!intval( $user_id )){
        return new WP_Error(
                  'jwt_auth_bad_config',
                  __('ID is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
                  array(
                    'status' => 403,
                  )
                );
      } else if (intval( $user_id )) {
        $user_id = ($user_id);
      }
      if($user_id != false) {
        $user = get_userdata($user_id);
      }

       $message_posts = get_posts( array(
            'numberposts' => -1,
            'meta_key'    => 'friends',
            'meta_value'  =>  ($user_id.'"'),
            'post_type'   => 'message',
            'meta_compare' => 'LIKE'
        ) );
      $msg = false;
      foreach ( $message_posts as $m ) {
        if($m->post_title == $title || $m->post_title == $title2){
          $msg = $m->ID;
        }
      }
      if($msg){
        return ['message_id' => $msg];
      }
      return [];
   }


  public function create_message($request){
    if($request->get_method() == "OPTIONS"){
      return array("result" => "Pre-flight checked");
    }

    $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
    $json = $request->get_json_params();
    $user_id = $email = $json['user_id'];
    $friend_id  = (string)($json['to']);
    $title = md5($user_id.$friend_id.'897sg8sd8g9sdg9');
    $title2 = md5($friend_id.$user_id.'897sg8sd8g9sdg9');

    /** First thing, check the secret key if not exist return a error*/
    if (!$secret_key || $friend_id == null || $user_id == null) {
      return new WP_Error(
        'jwt_auth_bad_config',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }

    /** Try to authenticate the user credentials*/
    if(!intval( $user_id )){
      return new WP_Error(
                'jwt_auth_bad_config',
                __('ID is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
                array(
                  'status' => 403,
                )
              );
    } else if (intval( $user_id )) {
      $user_id = ($user_id);
    }
    if($user_id != false) {
      $user = get_userdata($user_id);
    }

     $message_posts = get_posts( array(
          'numberposts' => -1,
          'meta_key'    => 'friends',
          'meta_value'  =>  ($user_id.'"'),
          'post_type'   => 'message',
          'meta_compare' => 'LIKE'
      ) );
    $msg = false;
    foreach ( $message_posts as $m ) {
      if($m->post_title == $title || $m->post_title == $title2){
        $msg = $m->ID;
      }
    }
    if($msg){
      return ['message_id' => $msg];
    }

    // Initialize the page ID to -1. This indicates no action has been taken.
    $post_id = -1;
    $slug = sanitize_title_with_dashes($title);

    // If the page doesn't already exist, then create it
    if( null == get_page_by_title( $title ) ) {
      // Set the post ID so that we know the post was created successfully
      $post_id = wp_insert_post(
        array(
          'comment_status'	=>	'open',
          'ping_status'		=>	'closed',
          'post_author'		=>	$user_id,
          'post_name'		=>	$slug,
          'post_title'		=>	$title,
          'post_status'		=>	'publish',
          'post_type'		=>	'message'
        )
      );

      /** add user meta */
      add_post_meta( $post_id, 'friends', [$user_id, $friend_id]);

    } else {
      return new WP_Error(
        'Already checked in!',
        __('Cannot creat a group', 'title-taken'),
        array(
          'status' => 403,
        )
      );
    } // end if

    return ['message_id' => $post_id];
  }




  /**
  	 * Returns Job Listings for Ajax endpoint.
  	 {
     "search_location":"Hudson",
     "gjm_use":"2",
     "gjm_element_id":"838",
     "gjm_lat":"44.9742223",
     "gjm_lng":"-92.7558609",
     "gjm_prev_address":"Hudson",
     "gjm_prefix":"gjm",
     "gjm_location_marker":"https://maps.google.com/mapfiles/ms/icons/red-dot.png",
     "gjm_radius":"50",
     "gjm_units":"imperial"
     }
  	 */
  public function 	get_listings($request) {
    global $wp_post_types;

    if($request->get_method() == "OPTIONS"){
      return array("result" => "Pre-flight checked");
    }
    $json = $request->get_json_params();

    if ( empty( $_REQUEST['form_data'] ) ) {
      $_REQUEST = $json;
      $_REQUEST['form_data'] = $json;
    }

    if(isset( $json['listing_type'] )){
      $json['filter_job_type'] = $json['listing_type'];
    }

    $result             = array();
    $search_location    = sanitize_text_field( stripslashes( $json['search_location'] ) );

    $search_keywords    = sanitize_text_field( stripslashes( $json['search_keywords'] ) );
    $search_categories  = isset( $json['search_categories'] ) ? $json['search_categories'] : '';
    $filter_job_types   = isset( $json['filter_job_type'] ) ? array_filter( array_map( 'sanitize_title', (array) $json['filter_job_type'] ) ) : null;
    $filter_post_status = isset( $json['filter_post_status'] ) ? array_filter( array_map( 'sanitize_title', (array) $json['filter_post_status'] ) ) : null;
    $types              = get_job_listing_types();
    $post_type_label    = $wp_post_types['job_listing']->labels->name;
    $orderby            = sanitize_text_field( $json['orderby'] );

    if ( is_array( $search_categories ) ) {
      $search_categories = array_filter( array_map( 'sanitize_text_field', array_map( 'stripslashes', $search_categories ) ) );
    } else {
      $search_categories = array_filter( array( sanitize_text_field( stripslashes( $search_categories ) ) ) );
    }

    $args = array(
      'search_location'    => $search_location,
      'search_keywords'    => $search_keywords,
      'search_categories'  => $search_categories,
      'job_types'          => is_null( $filter_job_types ) || sizeof( $types ) === sizeof( $filter_job_types ) ? '' : $filter_job_types + array( 0 ),
      'post_status'        => $filter_post_status,
      'orderby'            => $orderby,
      'order'              => sanitize_text_field( $json['order'] ),
      'offset'             => ( absint( $json['page'] ) - 1 ) * absint( $json['per_page'] ),
      'posts_per_page'     => absint( $json['per_page'] ),
    );

    if ( isset( $json['filled'] ) && ( $json['filled'] === 'true' || $json['filled'] === 'false' ) ) {
      $args['filled'] = $json['filled'] === 'true' ? true : false;
    }

    if ( isset( $json['featured'] ) && ( $json['featured'] === 'true' || $json['featured'] === 'false' ) ) {
      $args['featured'] = $json['featured'] === 'true' ? true : false;
      $args['orderby']  = 'featured' === $orderby ? 'date' : $orderby;
    }

    /**
     * Get the arguments to use when building the Job Listing WP Query.
     *
     * @since 1.0.0
     *
     * @param array $args Arguments used for generating Job Listing query (see `get_job_listings()`)
     */
    $jobs = get_job_listings( apply_filters( 'job_manager_get_listings_args', $args ) );

    $result = array(
      'found_jobs' => $jobs->have_posts(),
      'showing' => '',
      'max_num_pages' => $jobs->max_num_pages,
    );

    //var_dump($jobs); exit;

    if ( $jobs->post_count && ( $search_location || $search_keywords || $search_categories ) ) {
      $message = sprintf( _n( 'Search completed. Found %d matching record.', 'Search completed. Found %d matching records.', $jobs->found_posts, 'wp-job-manager' ), $jobs->found_posts );
      $result['showing_all'] = true;
    } else {
      $message = $jobs->post_count;
    }

    $search_values = array(
      'location'   => $search_location,
      'keywords'   => $search_keywords,
      'categories' => $search_categories,
    );

    /**
     * Filter the message that describes the results of the search query.
     *
     * @since 1.0.0
     *
     * @param string $message Default message that is generated when posts are found.
     * @param array $search_values {
     *  Helpful values often used in the generation of this message.
     *
     *  @type string $location   Query used to filter by job listing location.
     *  @type string $keywords   Query used to filter by general keywords.
     *  @type array  $categories List of the categories to filter by.
     * }
     */
    $result['showing'] = apply_filters( 'job_manager_get_listings_custom_filter_text', $message, $search_values );

    // Generate RSS link
    $result['showing_links'] = job_manager_get_filtered_links( array(
      'filter_job_types'  => $filter_job_types,
      'search_location'   => $search_location,
      'search_categories' => $search_categories,
      'search_keywords'   => $search_keywords,
    ) );

    /**
     * Send back a response to the AJAX request without creating HTML.
     *
     * @since 1.26.0
     *
     * @param array $result
     * @param WP_Query $jobs
     * @return bool True by default. Change to false to halt further response.
     */
    if ( true !== apply_filters( 'job_manager_ajax_get_jobs_html_results', true, $result, $jobs ) ) {
      /**
       * Filters the results of the job listing Ajax query to be sent back to the client.
       *
       * @since 1.0.0
       *
       * @param array $result {
       *  Package of the query results along with meta information.
       *
       *  @type bool   $found_jobs    Whether or not jobs were found in the query.
       *  @type string $showing       Description of the search query and results.
       *  @type int    $max_num_pages Number of pages in the search result.
       *  @type string $html          HTML representation of the search results (only if filter
       *                              `job_manager_ajax_get_jobs_html_results` returns true).
       *  @type array $pagination     Pagination links to use for stepping through filter results.
       * }
       */
      return wp_send_json( apply_filters( 'job_manager_get_listings_result', $result, $jobs ) );
    }

  	//ob_start();
		$arr = array();
		$arrUnique = array();
    $limit = 200;




  	if ( $result['found_jobs'] ) :
			while ( $jobs->have_posts() ) :
			  $jobs->the_post();
				foreach($jobs->posts as $post){

				if($limit == 0) {
				  break;
				}
				$limit--;


				if(in_array($post->ID, $arrUnique)) {
				  continue;
				}
				$arrUnique[] = $post->ID;

//          var_dump($post->ID);
//          continue;


          $extra = array();
          foreach($this->extraWhiteList as $ex => $label) {
              if(intval(get_post_meta($post->ID, $ex, true)) == 1){
                $e = [ 'name' => $label];
                if($ex == 'valet'){
                  $e[] = ['icon' => 'md-person'];
                }
                $extra[] = $e;
              }
          }

          $events = array();
          $contests = array();
          $freeFun = array();

          foreach($this->eventWhiteList as $arr_name => $ev) {
              $eventRow = get_post_meta($post->ID, $ev, true);
              foreach($eventRow as $i => $row) {
                array_push($$arr_name,[
                   'name' => get_post_meta($post->ID, $ev.'_'.$i.'_name', true),
                   'date' => get_post_meta($post->ID, $ev.'_'.$i.'_date', true)
                 ]);
              }
          }

          $hours = array();
          $open = false;
          $hoursRow = get_post_meta($post->ID, 'hours', true);
          foreach($hoursRow as $i => $row) {
            $d=get_post_meta($post->ID, 'hours'.'_'.$i.'_time_group', true);
            $h=get_post_meta($post->ID, 'hours'.'_'.$i.'_time', true);

            $explodeDay = explode(' - ', $d);
            $dayCheck = false;
            if(count($explodeDay) > 1 && $open == false){
              $daysOfWeek = array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
              $today = date('D');
              $checkStart = false;
              foreach($daysOfWeek as $dd) {
               // echo($explodeDay[0].','.$explodeDay[1].','.$dd.','.$today);
                if($dd == $explodeDay[0] && $today == $dd){
                  $dayCheck = true;
                  break;
                }else if($checkStart == true && $today == $dd){
                  $dayCheck = true;
                  break;
                }else if($dd == $explodeDay[0]) {
                    $checkStart = true;
                }
                if($dd == $explodeDay[1]){
                  break;
                }
              }
            }

            $explodeTime = explode(' - ', $h);
            $timeCheck = false;
            if(count($explodeDay) > 1 && count($explodeTime) > 1 && $open == false){
              $daysOfWeek = array("12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am","8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm","8pm", "9pm", "10pm", "11pm");
              $today = date('ga');
              $checkStart = false;
              foreach($daysOfWeek as $dd) {
               // echo($explodeTime[0].','.$explodeTime[1].','.$dd.','.$today);
                if($dd == $explodeTime[0] && $today == $dd){
                  $timeCheck = true;
                  break;
                }else if($checkStart == true && $today == $dd){
                  $timeCheck = true;
                  break;
                }else if($dd == $explodeTime[0]) {
                    $checkStart = true;
                }
                if($dd == $explodeTime[1]){
                  break;
                }
              }
            }



            if($dayCheck && $timeCheck) {
              $open = true;
            }

            $hours[] = [
              'days' => $d,
              'hours' => $h
            ];
          }

          $gallery = array();
          $galleryImages = get_post_meta($post->ID, 'gallery', true);
          foreach($galleryImages as $i => $row) {
            $attachment = get_post( $row );
            $photo = wp_get_attachment_image_src($row, 'large');
            if(!empty($photo))
              $gallery[] = [
                'image' => $photo[0],
                'description' => $attachment->post_content
              ];
          }

          $specials = array();
          $specialsRow = get_post_meta($post->ID, 'special_products', true);
          //var_dump($specialsRow); exit;
          foreach($specialsRow as $i => $row) {
              $specials[] = [
                'price' => get_post_meta($row, '_price', true ),
                'name' => get_the_title($row)
              ];
          }


          $friends = array();
          $friendsRow = get_post_meta($post->ID, 'friends', true);
          foreach($friendsRow as $i => $row) {
            $u = get_userdata($row);
            $f = [
              'name' => $u->display_name,
              'id' => $u->ID
            ];
            if (strlen(wp_user_avatars_get_local_avatar_url($u->ID, 480)) > 0) {
               $f['avatar']= wp_user_avatars_get_local_avatar_url($u->ID, 480);
            }else {
              $f['avatar'] = get_avatar_url($u->ID);
            }
            $friends[] = $f;
          }

          //if($post->ID == 3645)
          //var_dump($post->ID, $gallery); exit;

          $char = get_post_meta($post->ID, '_company_video', true);
          preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $char, $matches);
          $charURL = $matches[1];


          $arr[] = array(
              'ID'    => $post->ID,
              'description'    => $post->post_content,
              'author'    => intval($post->post_author),
              'title'  => $post->post_title,
              'status'        => $post->post_status,
              'guid'            => $post->guid,
              'address'              => $post->formatted_address,
              'distance' => $post->distance,
              'lat'             => $post->lat,
              'long'     => $post->long,
              'tags' => (the_tags('') !== null ? the_tags('') : ''),
              'image' => get_the_post_thumbnail_url($post),
              'vip' => (intval(get_post_meta($post->ID, '_featured', true)) == 0 ? false : true),
              'character' => 'http://www.youtube.com/embed/' . $charURL,
              'open' => $open,
              'extras' => $extra,
              'events' => $events,
              'contests' => $contests,
              'freeFun' => $freeFun,
              'hours' => $hours,
              'photos' => $gallery,
              'menu' => [],
              'specials' => $specials,
              'members' => $friends,
              'website' => get_post_meta($post->ID, '_company_website', true),

          );
//          var_dump($arr); exit;
//          break;
        }
  		endwhile;
		endif;

  	$result['html'] = $arr;

    // Generate pagination
    if ( isset( $json['show_pagination'] ) && $json['show_pagination'] === 'true' ) {
      $result['pagination'] = get_job_listing_pagination( $jobs->max_num_pages, absint( $json['page'] ) );
    }

    /** This filter is documented in includes/class-wp-job-manager-ajax.php (above) */
    wp_send_json( apply_filters( 'job_manager_get_listings_result', $result, $jobs ) );
  }


  public function get_products($request)	{

     $user_id = $request->get_param('id') ? $request->get_param('id') : $request->get_param('author');
     if(!$user_id){
      return new WP_Error(
        'jwt_auth_bad_config',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
     }
     if($user_id == 'HopM'){ //hopMenu
        $user_id = 1;
     }
     if($user_id == 'HopE'){ //hopEvents
       $user_id = 2;
     }
     if($user_id == 'HopV'){ //hopeVIP
       $user_id = 3;
     }

     $shortHand = $request->get_param('shortHand') ? true : false;
     $full_product_list = self::_get_products($user_id, $shortHand);
     return wp_send_json($full_product_list);
  }

  public function _get_products($user_id = null, $shortHand = false) {

     $full_product_list = array();
     if($user_id == null){
      $loop = new WP_Query( array( 'post_type' => array('product', 'product_variation'), 'posts_per_page' => -1 ) );
     } else {
      $loop = new WP_Query( array( 'post_type' => array('product', 'product_variation'), 'posts_per_page' => -1, 'author' => $user_id ) );
     }

     	while ( $loop->have_posts() ) : $loop->the_post();
     		$theid = get_the_ID();
     		$product = new WC_Product($theid);
     		// its a variable product
     		if( get_post_type() == 'product_variation' ){
     			$parent_id = wp_get_post_parent_id($theid );
     			$sku = get_post_meta($theid, '_sku', true );
     			$thetitle = get_the_title( $parent_id);

         // ****** Some error checking for product database *******
                 // check if variation sku is set
                 if ($sku == '') {
                     if ($parent_id == 0) {
                 		// Remove unexpected orphaned variations.. set to auto-draft
                 		$false_post = array();
                         $false_post['ID'] = $theid;
                         $false_post['post_status'] = 'auto-draft';
                         wp_update_post( $false_post );
                         if (function_exists(add_to_debug)) add_to_debug('false post_type set to auto-draft. id='.$theid);
                     } else {
                         // there's no sku for this variation > copy parent sku to variation sku
                         // & remove the parent sku so the parent check below triggers
                         $sku = get_post_meta($parent_id, '_sku', true );
                         if (function_exists(add_to_debug)) add_to_debug('empty sku id='.$theid.'parent='.$parent_id.'setting sku to '.$sku);
                         update_post_meta($theid, '_sku', $sku );
                         update_post_meta($parent_id, '_sku', '' );
                     }
                 }
      	// ****************** end error checking *****************

         // its a simple product
         } else {
             $price = get_post_meta($theid, '_price', true );
             $rating = get_post_meta($theid, '_wc_average_rating', true );
             $stock = get_post_meta($theid, '_stock_status', true );
             $thetitle = get_the_title();
             $cat = wp_get_post_terms($theid, 'product_cat');
             $categories = []; //array to store all of your category names

             foreach($cat as $category_list_item){
                 if(! empty($category_list_item->name) ){
                     array_push($categories, $category_list_item->name);
                 }
             }
         }
         // add product to array but don't add the parent of product variations
         if ($thetitle && !$shortHand){
            $full_product_list[] = array(
              "inStock" => ($stock == "instock" ? true : false),
              "rating" => $rating,
              "title" => $thetitle,
              "price" => intval($price ? $price : 0),
              "id" => $theid,
              "category" => $categories
            );
         } else if ($thetitle && $shortHand) {
             $full_product_list[] = array(
              "inStock" => ($stock == "instock" ? true : false),
              "name" => $thetitle,
              "price" => intval($price ? $price : 0),
              "id" => $theid
            );
         }
      endwhile;

      wp_reset_query();
      // sort into alphabetical order, by title
      sort($full_product_list);
      return $full_product_list;

  }


  public function get_orders($request) {
    $user_id = $request->get_param('id') ? $request->get_param('id') : $request->get_param('author');
    $getHash = $request->get_param('key') ? $request->get_param('key') : false;
    $k = ("dfgd33" . $user_id . "9h3hgs3462235" . $user_id . "28498823985203975834");
    if($getHash && $getHash != $k) {
      $getHash = false;
    }
    if(!$user_id && !$getHash){
      return new WP_Error(
        'jwt_auth_bad_config',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
     }

    // Get all customer orders
    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $user_id,
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
    ) );

    foreach($customer_orders as $o){
      $hash = self::doOrderHash($o);
      $o->hash = $hash;

      $or = new WC_Order($o->ID);
      $total = 0;
      $o->post_basket = [];
      $shop = 0;
      foreach ($or->get_items() as $key => $lineItem) {
        $total++;
        $price = get_post_meta($lineItem['product_id'], '_price', true );
        $o->post_basket[] = [
          'id' => $lineItem['product_id'],
          'name' => $lineItem['name'],
          'price' => floatval($price)
        ];
        $shop = get_post($lineItem['product_id'])->post_author;
      }

      $args = array(
      	'post_type'        => 'job_listing',
      	'post_status'      => 'publish',
      	'post_author'      => intval($shop),
      	'suppress_filters' => true
      );
      $posts_array = reset(get_posts( $args ));

      $o->post_company = $posts_array->post_title ? $posts_array->post_title : 'Company ID: ' . $shop;
      $o->post_items = $total;
    }

    return wp_send_json($customer_orders);
  }



  public function create_order($request) {

    global $woocommerce;

    if($request->get_method() == "OPTIONS"){
      return array("result" => "Pre-flight checked");
    }

    $user_id = $request->get_param('id') ? $request->get_param('id') : $request->get_param('author');
    $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
    /** First thing, check the secret key if not exist return a error*/
    if (!$secret_key || $user_id == false || $user_id == null) {
      return new WP_Error(
        'jwt_auth_bad_config',
        __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
        array(
          'status' => 403,
        )
      );
    }

    $products = $request->get_param('products');
    $dollar = $request->get_param('dollar');

    $address = array(
     'first_name' => get_post_meta( $user_id, 'billing_name', true ),
     'last_name'  => get_post_meta( $user_id, '_billing_last_name', true ),
     'company'    => get_post_meta( $user_id, '_billing_company', true ),
     'email'      => get_post_meta( $user_id, '_billing_email', true ),
     'phone'      => get_post_meta( $user_id, 'phone', true ),
     'address_1'  => get_post_meta( $user_id, 'billing_address_1', true ),
     'address_2'  => get_post_meta( $user_id, 'billing_address_2', true ),
     'city'       => get_post_meta( $user_id, 'billing_city', true ),
     'state'      => get_post_meta( $user_id, 'billing_state', true ),
     'postcode'   => get_post_meta( $user_id, 'billing_postcode', true ),
     'country'    => get_post_meta( $user_id, 'billing_country', true ),
    );

    if($dollar){
      $total = 0;
      foreach($products as $product){
        $p = get_product($product);
        $total = $total + floatval($p->price);
      }

      $fund = get_user_meta($user_id, 'hop_current_fund', true);
      $newFund = floatval($fund) - floatval($total);

      if($fund == 0 || $total == 0 || $total > $fund) {
        return new WP_Error(
          'jwt_auth_bad_config',
          __('Not enough funds', 'wp-api-jwt-auth'),
          array(
            'status' => 401,
          )
        );
      }
    }

    // Now we create the order
    $args = array(
        'status'        => "Completed",
        'customer_id'   => $user_id,
        'created_via'   => 'App Order',
      );
    $order = wc_create_order($args);

    foreach($products as $product){
    // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
      $order->add_product( get_product($product), 1); // This is an existing SIMPLE product
    }
    $order->set_address( $address, 'billing' );
    // Update post
      $my_post = array(
          'ID'           => $order->ID,
          'post_content' => ($dollar ? '$' : 'P'),
      );

    // Update the post into the database
      wp_update_post( $my_post );
    //
    $order->calculate_totals();
    $order->update_status("Completed", 'Imported order by:'.($dollar ? 'DOLLARS' : 'POINTS'), TRUE);
    $order->save();
    // Get all customer orders
    $hash = self::doOrderHash($order);
    $customer_orders = ['hash' => $hash];

    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $user_id,
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
    ) );

    foreach($customer_orders as $o){
      if($o->ID == $order->ID){
        $customer_orders = $o;

        $or = new WC_Order($o->ID);
        $total = 0;
        $o->post_basket = [];
        $shop = 0;
        foreach ($or->get_items() as $key => $lineItem) {
          $total++;
          $price = get_post_meta($lineItem['product_id'], '_price', true );
          $o->post_basket[] = [
            'id' => $lineItem['product_id'],
            'name' => $lineItem['name'],
            'price' => floatval($price)
          ];
          $shop = get_post($lineItem['product_id'])->post_author;
        }

        $args = array(
          'post_type'        => 'job_listing',
          'post_status'      => 'publish',
          'post_author'      => intval($shop),
          'suppress_filters' => true
        );
        $posts_array = reset(get_posts( $args ));

        $o->post_company = $posts_array->post_title ? $posts_array->post_title : 'Company ID: ' . $shop;
        $o->post_items = $total;
        break;
      }
    }

    $hash = self::doOrderHash($order);
    $customer_orders->hash = $hash;

    //updateFund
    update_user_meta($user_id, 'hop_current_fund', $newFund );

    return wp_send_json($customer_orders);
  }

  private function doOrderHash($order){
    if(!$order || !$order->ID)
      return 'false';
    return md5(md5($order->ID).$this->salt);
  }

  public function check_order_hash($request){

      global $woocommerce;

      if($request->get_method() == "OPTIONS"){
        return array("result" => "Pre-flight checked");
      }

      $user_id = $request->get_param('id') ? $request->get_param('id') : $request->get_param('author');
      $date = $request->get_param('date') ? $request->get_param('date') : $request->get_param('by');
      $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
      /** First thing, check the secret key if not exist return a error*/
      if (!$secret_key || $user_id == false || $user_id == null) {
        return new WP_Error(
          'jwt_auth_bad_config',
          __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
          array(
            'status' => 403,
          )
        );
      }

     $hash = $request->get_param('hash');
     $retn = false;

     $customer_orders = get_posts( array(
         'numberposts' => -1,
         'meta_key'    => '_customer_user',
         'meta_value'  => $user_id,
         'post_type'   => wc_get_order_types(),
         'post_status' => array_keys( wc_get_order_statuses() ),
     ) );

     foreach($customer_orders as $key=>$o){
       if($hash == self::doOrderHash($o)){
         $customer_orders = $o;
         $checked = get_post_meta($o->ID, 'hash_checked', true );
         if(!$checked)
            $retn = true;
         else{
            $retn = false;
             $o->err = "Already Checked";
         }
         $or = new WC_Order($o->ID);
         $total = 0;
         $o->post_basket = [];
         $shop = 0;
         foreach ($or->get_items() as $key => $lineItem) {
           $total++;
           $price = get_post_meta($lineItem['product_id'], '_price', true );
           $o->post_basket[] = [
             'id' => $lineItem['product_id'],
             'name' => $lineItem['name'],
             'price' => floatval($price)
           ];
           $shop = get_post($lineItem['product_id'])->post_author;
         }

         $args = array(
           'post_type'        => 'job_listing',
           'post_status'      => 'publish',
           'post_author'      => intval($shop),
           'suppress_filters' => true
         );
         $posts_array = reset(get_posts( $args ));

         $o->post_company = $posts_array->post_title ? $posts_array->post_title : 'Company ID: ' . $shop;
         $o->post_items = $total;
         break;
       }else {
       unset($customer_orders[$key]);
       }
     }

    if(count($customer_orders) == 0 ){
      return new WP_Error(
        'No order by that user',
        __('Order not recognised', 'wp-order-jwt-auth'),
        array(
          'status' => 401,
        )
      );
    }

     $customer_orders->hash = $hash;
     $customer_orders->confirm = $retn;

     //updateChecked
     update_post_meta($customer_orders->ID, 'hash_checked', 'true' );

     return wp_send_json($customer_orders);
  }

	/**
	 * This is our Middleware to try to authenticate the user according to the
	 * token send.
	 *
	 * @param (int|bool) $user Logged User ID
	 *
	 * @return (int|bool)
	 */
	public function determine_current_user($user)
	{
		/**
		 * This hook only should run on the REST API requests to determine
		 * if the user in the Token (if any) is valid, for any other
		 * normal call ex. wp-admin/.* return the user.
		 *
		 * @since 1.2.3
		 **/
		$rest_api_slug = rest_get_url_prefix();
		$valid_api_uri = strpos($_SERVER['REQUEST_URI'], $rest_api_slug);
		if(!$valid_api_uri){
			return $user;
		}

		/*
		 * if the request URI is for validate the token don't do anything,
		 * this avoid double calls to the validate_token function.
		 */
		$validate_uri = strpos($_SERVER['REQUEST_URI'], 'token/validate');
		if ($validate_uri > 0) {
			return $user;
		}

		$token = $this->validate_token(false);

		if (is_wp_error($token)) {
			if ($token->get_error_code() != 'jwt_auth_no_auth_header') {
				/** If there is a error, store it to show it after see rest_pre_dispatch */
				$this->jwt_error = $token;
				return $user;
			} else {
				return $user;
			}
		}
		/** Everything is ok, return the user ID stored in the token*/
		return $token->data->user->id;
	}

	/**
	 * Main validation function, this function try to get the Autentication
	 * headers and decoded.
	 *
	 * @param bool $output
	 *
	 * @return WP_Error | Object
	 */
	public function validate_token($output = true)
	{
		/*
		 * Looking for the HTTP_AUTHORIZATION header, if not present just
		 * return the user.
		 */
		$auth = isset($_SERVER['HTTP_AUTHORIZATION']) ?  $_SERVER['HTTP_AUTHORIZATION'] : false;


		/* Double check for different auth header string (server dependent) */
		if (!$auth) {
			$auth = isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) ?  $_SERVER['REDIRECT_HTTP_AUTHORIZATION'] : false;
		}

		if (!$auth) {
			return new WP_Error(
				'jwt_auth_no_auth_header',
				__('Authorization header not found.', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}

		/*
		 * The HTTP_AUTHORIZATION is present verify the format
		 * if the format is wrong return the user.
		 */
		list($token) = sscanf($auth, 'Bearer %s');
		if (!$token) {
			return new WP_Error(
				'jwt_auth_bad_auth_header',
				__('Authorization header malformed.', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}

		/** Get the Secret Key */
		$secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
		if (!$secret_key) {
			return new WP_Error(
				'jwt_auth_bad_config',
				__('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
				array(
					'status' => 403,
				)
			);
		}

		/** Try to decode the token */
		try {
			$token = JWT::decode($token, $secret_key, array('HS256'));
			/** The Token is decoded now validate the iss */
			if ($token->iss != get_bloginfo('url')) {
				/** The iss do not match, return error */
				return new WP_Error(
					'jwt_auth_bad_iss',
					__('The iss do not match with this server', 'wp-api-jwt-auth'),
					array(
						'status' => 403,
					)
				);
			}
			/** So far so good, validate the user id in the token */
			if (!isset($token->data->user->id)) {
				/** No user id in the token, abort!! */
				return new WP_Error(
					'jwt_auth_bad_request',
					__('User ID not found in the token', 'wp-api-jwt-auth'),
					array(
						'status' => 403,
					)
				);
			}
			/** Everything looks good return the decoded token if the $output is false */
			if (!$output) {
				return $token;
			}
			/** If the output is true return an answer to the request to show it */
			return array(
				'code' => 'jwt_auth_valid_token',
				'data' => array(
					'status' => 200,
				),
			);
		} catch (Exception $e) {
			/** Something is wrong trying to decode the token, send back the error */
			return new WP_Error(
				'jwt_auth_invalid_token',
				$e->getMessage(),
				array(
					'status' => 403,
				)
			);
		}
	}

	public function sendPushNotification($include_player_ids = []) {
	  https://stackoverflow.com/questions/43529971/sending-push-notifications-via-onesignal-using-php
	   $content = array(
     			"en" => 'English Message'
     			);

     		$fields = array(
     			'app_id' => "d1185e44-0e81-4c91-88ce-755ab7d7550c",
     			'include_player_ids' => array("6392d91a-b206-4b7b-a620-cd68e32c3a76","76ece62b-bcfe-468c-8a78-839aeaa8c5fa","8e0f21fa-9a5a-4ae7-a9a6-ca1f24294b86"),
     			'data' => array("foo" => "bar"),
     			'contents' => $content
     		);

     		$fields = json_encode($fields);
         	print("\nJSON sent:\n");
         	print($fields);

     		$ch = curl_init();
     		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
     		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
     												   'Authorization: Basic NzdkMDY5YTYtM2U4My00ZDhhLTg4OGUtNGQwNmRmMTNkNGJh'));
     		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
     		curl_setopt($ch, CURLOPT_HEADER, FALSE);
     		curl_setopt($ch, CURLOPT_POST, TRUE);
     		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

     		$response = curl_exec($ch);
     		curl_close($ch);

     		return $response;

	}

	private function createPushDevice($fields = []){
    $fields = array(
    'app_id' => "d1185e44-0e81-4c91-88ce-755ab7d7550c",
    'identifier' => "ce777617da7f548fe7a9ab6febb56cf39fba6d382000c0395666288d961ee566",
    'language' => "en",
    'timezone' => "-28800",
    'game_version' => "1.0",
    'device_os' => "9.1.3",
    'device_type' => "0",
    'device_model' => "iPhone 8,2",
    'tags' => array("release" => "launch")
    );

    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    $return["allresponses"] = $response;
    $return = json_encode( $return);

	}

//	deploy andriod:
//	ionic cordova build --release android
//
//	jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore ./platforms/android/build/outputs/apk/android-release-unsigned.apk alias_name
//
//  cd /Users/jasonpickering/Library/Android/sdk/build-tools/25.0.2
//  ./zipalign -v 4 /Users/Projects/Hop/HopApp/platforms/android/build/outputs/apk/android-release-unsigned.apk /Users/Projects/Hop/HopApp/Hop.apk


	/**
	 * Filter to hook the rest_pre_dispatch, if the is an error in the request
	 * send it, if there is no error just continue with the current request.
	 *
	 * @param $request
	 */

	public function rest_pre_dispatch($request)
	{
		if (is_wp_error($this->jwt_error)) {
			return $this->jwt_error;
		}
		return $request;
	}
}
?>
