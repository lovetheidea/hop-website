<?php
/**
 * Roots includes
 */
include( get_template_directory() . '/lib/init.php');            // Initial theme setup and constants
include( get_template_directory() . '/lib/wrapper.php');         // Theme wrapper class
include( get_template_directory() . '/lib/config.php');          // Configuration
include( get_template_directory() . '/lib/activation.php');      // Theme activation
include( get_template_directory() . '/lib/titles.php');          // Page titles
include( get_template_directory() . '/lib/cleanup.php');         // Cleanup
include( get_template_directory() . '/lib/nav.php');             // Custom nav modifications
include( get_template_directory() . '/lib/comments.php');        // Custom comments modifications
include( get_template_directory() . '/lib/widgets.php');         // Sidebars and widgets
include( get_template_directory() . '/lib/scripts.php');         // Scripts and stylesheets
include( get_template_directory() . '/lib/custom.php');          // Custom functions

// THEMOVATION CUSTOMIZATION
include( get_template_directory() . '/lib/class-tgm-plugin-activation.php');    // Bundled Plugins

// Activate Option Tree in the theme rather than as a plugin
add_filter( 'ot_theme_mode', '__return_true' );
add_filter( 'ot_show_pages', '__return_false' );
//add_filter( 'ot_show_pages', '__return_true' );

// Don't use Option Tree Meta Boxes for Pages.
if(isset($_GET['post']) && $_GET['post'] > ""){
    if(get_post_type($_GET['post']) == 'page'){
        add_filter('ot_meta_boxes', '__return_false' );
    }
}


include_once(get_template_directory() . '/option-tree/ot-loader.php');
include_once(get_template_directory() . '/option-tree/theme-options.php' ); // LOAD THEME SETTINGS
include_once(get_template_directory() . '/option-tree/theme-options-defaults.php'); // LOAD OT DEFAULTS
include_once(get_template_directory() . '/register-meta-boxes.php'); // LOAD OT DEFAULTS

// END Option Tree


// Display 24 products per page. Goes in functions.php
/*add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 6;' ), 20 );

add_filter ('woocommerce_add_to_cart_redirect', 'woo_redirect_to_checkout');

function woo_redirect_to_checkout() {
    $checkout_url = WC()->cart->get_cart_url();
    return $checkout_url;
}*/

add_action( 'template_redirect', function() {

    if( ( is_page('feedback') ) ) {

        if (!is_user_logged_in() ) {
            wp_redirect( site_url( '/' ) );
            exit();
        }

    }

});

function sb_add_cpts_to_api( $args, $post_type ) {
    if ( 'group' === $post_type ) {
        $args['show_in_rest'] = true;
        $args['rest_base'] = 'group';
        // $args['rest_controller_class'] = 'WP_REST_Posts_Controller';
    }
    return $args;
}
add_filter( 'register_post_type_args', 'sb_add_cpts_to_api', 10, 2 );


function filter_rest_allow_anonymous_comments() {
    return true;
}
add_filter('rest_allow_anonymous_comments','filter_rest_allow_anonymous_comments');


// Enable the option show in rest
add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );

// Enable the option edit in rest
add_filter( 'acf/rest_api/field_settings/edit_in_rest', '__return_true' );


add_filter( 'rest_checkin_query', function( $args ) {
   if(isset($_GET['restaurant'])){
	$get = !empty($_GET['restaurant']) && is_numeric($_GET['restaurant']) ? intval(esc_sql($_GET['restaurant'])) : 'XX1X';
	$arr = !empty($_GET['restaurant']) ? array('key'   => 'restaurant','value' =>  $get, 'compare' => '=') : array('key'   => 'restaurant');

$args['meta_query'] = array(
		$arr
	);
}

	return $args;
} );


add_filter('rest_prepare_checkin', 'json_api_encode_acf', 12, 3);
function json_api_encode_acf($res, $post, $context) {
   if (isset($res->data)) {
      if (isset($res->data['acf']['restaurant'])) {
            $res->data['acf']['restaurant_avatar'] = get_the_post_thumbnail_url($res->data['acf']['restaurant']);
         }
	if (isset($res->data['acf']['long_lat'])) {
	    $lex = explode(',', $res->data['acf']['long_lat']);
            $res->data['acf']['longitude'] = $lex[0];
	    $res->data['acf']['latitude'] = $lex[1];
         }
      return $res;
   }
}

add_filter( 'rest_message_query', function( $args ) {
   if(isset($_GET['friend'])){
	$get = !empty($_GET['friend']) && is_numeric($_GET['friend']) ? intval(esc_sql($_GET['friend'])) : 'XX1X';
	$arr = !empty($_GET['friend']) ? array('key'   => 'friends','value' =>  ( $get.'"'), 'compare' => 'LIKE') : array('key'   => 'XX1X');

$args['meta_query'] = array(
		$arr
	);
}
	return $args;
} );

add_filter( 'rest_post_query', function( $args ) {
   if(isset($_GET['friend'])){
	$get = !empty($_GET['friend']) && is_numeric($_GET['friend']) ? intval(esc_sql($_GET['friend'])) : 'XX1X';
	$arr = !empty($_GET['friend']) ? array('key'   => 'friends','value' =>  ( $get.'"'), 'compare' => 'LIKE') : array('key'   => 'XX1X');

$args['meta_query'] = array(
		$arr
	);
}
	return $args;
} );

add_filter('rest_prepare_message', 'json_api_encode_acfm', 12, 3);
function json_api_encode_acfm($res, $post, $context) {
global $wpdb; 
   if (isset($res->data)) {
      if (isset($res->data['id'])) {
           
  	$id = mysql_escape_string($res->data['id']); // get id of post somewhere
  	$sql = "
   	SELECT DISTINCT SUBSTRING(comment_content,1,100) AS com_excerpt 
   	FROM $wpdb->comments 
   	WHERE comment_approved = '1' AND comment_post_ID = {$id}
   	ORDER BY comment_date_gmt DESC 
   	LIMIT 1";   
 	$comments = $wpdb->get_results($sql); 
	
	$res->data['comment'] = !empty($comments) ? $comments[0]->com_excerpt : '';
	
	if (isset($res->data['acf'])) {
		if (isset($res->data['acf']['friends'])) {
			foreach($res->data['acf']['friends'] as $k => $f){
				if(isset($f['ID'])){
					continue;
				}
				$u = get_userdata($f);
				$data = [
					"ID" => $u->ID,
          				"user_firstname"=> $u->first_name,
         				"user_lastname"=> $u->last_name,
          				"nickname"=> $u->nickname,
          				"user_nicename"=> $u->user_nicename,
          				"display_name"=> $u->display_name,
          				"user_email"=> $u->user_email,
          				"user_url"=> $u->user_url,
          				"user_registered"=> $u->user_registered,
          				"user_description"=> $u->description
         			];
				$data['user_avatar']=  "<img alt='' src='";
				if (strlen(wp_user_avatars_get_local_avatar_url($u->ID, 480)) > 0) {
               				$data['user_avatar']= $data['user_avatar'].wp_user_avatars_get_local_avatar_url($u->ID, 480);
            			}else {
              				$data['user_avatar'] = $data['user_avatar'].get_avatar_url($u->ID);
           			}
				$data['user_avatar']=  $data['user_avatar']. "' class='avatar avatar-96 photo' height='96' width='96' />";
        
				$res->data['acf']['friends'][$k] = $data;
			}
		}

	}
      }
      return $res;
   }
}

add_filter('rest_prepare_group', 'json_api_encode_acfm_group', 12, 3);
function json_api_encode_acfm_group($res, $post, $context) {
global $wpdb; 
   if (isset($res->data)) {
      if (isset($res->data['id'])) {
	
	if (isset($res->data['acf'])) {
		if (isset($res->data['acf']['friends'])) {
			foreach($res->data['acf']['friends'] as $k => $f){
				if(!isset($f['ID'])){
					continue;
				}
				$u = get_userdata($f['ID']);
				
				if (strlen(wp_user_avatars_get_local_avatar_url($f['ID'], 480)) > 0) {
               				$res->data['acf']['friends'][$k]['avatar']= wp_user_avatars_get_local_avatar_url($f['ID'], 480);
            			}else {
              				$res->data['acf']['friends'][$k]['avatar'] = get_avatar_url(f['ID']);
           			}
        
			}
		}

	}
      }
      return $res;
   }
}


add_filter('rest_prepare_comment', 'json_api_encode_acfm_comment', 12, 3);
function json_api_encode_acfm_comment($res, $post, $context) {
global $wpdb; 
   if (isset($res->data)) {
      if (isset($res->data['author'])) {
	$userID = $res->data['author'];
	$m = get_user_meta($userID, 'push_token', true);
	$res->data['push_token'] = $m ? $m : '';
      }
   if (isset($res->data['post'])) {
	$post = $res->data['post'];
	$title = get_the_title($post);
	$res->data['post_title'] = $title ? $title : '';
      }
      return $res;
   }
}

