<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
/* Add your JavaScript code here.*/

function doThis($el, i, that){
  var _i = i;
  var _that = that;
  var $ = jQuery
  $el.parents('ul').find('li').fadeTo("fast", 0.4);
  $el.parents('ul').find('li p').fadeTo("fast", 0);
  $el.parents('ul').find('li').css('filter', 'grayscale(1) blur(2px)');
  $el.find('p').fadeTo("fast", 1);
  $el.css('filter', 'grayscale(0) blur(0px)');
  $el.fadeTo("fast", 1, function() {
    if(_i == 3){
      	setTimeout(function () { setTimerOff($, _that); }, 4000);
   		
  	}
  });
}

function setTimerOff($, that){
  that.css("opacity", 0.4);
  that.each(function (i) {
    var $li = $(this);
    if(i ==0)
     	doThis($li, i, that);
    else
    	setTimeout(function () { doThis($li, i, that); }, 4000 * (i));
  });
}


jQuery(document).ready(function( $ ){
  if($(".page-id-3229 #themo_thumb_slider_1_inner ul li").length)
 	setTimerOff($, $(".page-id-3229 #themo_thumb_slider_1_inner ul li"));
  if($(".home #themo_thumb_slider_1_inner ul li").length)
 	setTimerOff($, $(".home #themo_thumb_slider_1_inner ul li"));
  
  var $sidebar = $('.sidebar div.job_listings');
  if($sidebar.length){
    $window = $(window);
    offset = $sidebar.offset();
    topPadding = 0;

    $window.scroll(function() {
        if ($window.scrollTop() > (offset.top - 90)) {
            $sidebar.css({
                'top': '90px',
                'position': 'fixed'
            });
        } else {
            $sidebar.css({
                'top': '',
                'position': ''
            });
        }
    });
  }
  
  var middleSingle = $('.single-post .main.col-sm-8').parent();
  if(middleSingle.length){
    var x = $('.single-post .main.col-sm-8').width() + 350;
    $('.single-post .inner-container .container>.row').css('width', x);
  }
  
   $(window).on('beforeunload', function(){
         $('body.loaded').removeClass('loaded');
     });
  
});


</script>
<!-- end Simple Custom CSS and JS -->
